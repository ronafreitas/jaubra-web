<div class="user-info">
    <div class="image">
        <img src="<?=HOST;?>images/user.png" width="48" height="48" alt="User" />
    </div>
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Usuário</div>
        <div class="email">email@examplo.com</div>
    </div>
</div>

<?php $page = explode('/', $_SERVER['REQUEST_URI']);?>
<div class="menu">
    <ul class="list">
        <li <?php echo isset($page[2]) and $page[2] == 'home' ? 'class="active"' : '';?>>
            <a href="<?=HOST;?>home">
                <img src="<?=HOST;?>images/home.png" height="25px">
                <span <?php echo (isset($page[2]) and $page[2] == 'home') ? 'style="color:#2E86AB"' : 'style="color:#595758"';?>>Home</span>
            </a>
        </li>      
        <li <?php echo isset($page[2]) and $page[2] == 'mensagens' ? 'class="active"' : '';?>>
            <a href="<?=HOST;?>mensagens">
                <img src="<?=HOST;?>images/mensagens.png" height="25px">
                <span <?php echo (isset($page[2]) and $page[2] == 'mensagens') ? 'style="color:#2E86AB"' : 'style="color:#595758"';?>>Mensagens</span>
            </a>
        </li>
        <li <?php echo isset($page[2]) and $page[2] == 'dadoscadastrais' ? 'class="active"' : '';?>>
            <a href="<?=HOST;?>dadoscadastrais">
                <img src="<?=HOST;?>images/dadoscadastrais.png" height="25px">
                <span <?php echo (isset($page[2]) and $page[2] == 'dadoscadastrais') ? 'style="color:#2E86AB"' : 'style="color:#595758"';?>>Dados Cadastrais</span>
            </a>
        </li>
        <li <?php echo isset($page[2]) and $page[2] == 'fale-conosco' ? 'class="active"' : '';?>>
            <a href="<?=HOST;?>fale-conosco">
                <img src="<?=HOST;?>images/fale-conosco.png" height="25px">
                <span <?php echo (isset($page[2]) and $page[2] == 'fale-conosco') ? 'style="color:#2E86AB"' : 'style="color:#595758"';?>>Fale Conosco</span>
            </a>
        </li>

        <li <?php echo isset($page[2]) and $page[2] == 'relatorios' ? 'class="active"' : '';?>>
            <a href="#">
                <img src="<?=HOST;?>images/relatorios.png" height="25px">
                <span <?php echo (isset($page[2]) and $page[2] == 'relatorios') ? 'style="color:#2E86AB"' : 'style="color:#595758"';?>>Relatorios</span>
            </a>
        </li>

        <?php if(isset($_SESSION['per']) AND $_SESSION['per'] == 0):?>
            <li <?php echo isset($page[2]) and $page[2] == 'administradores' ? 'class="active"' : '';?>>
                <a href="<?=HOST;?>administradores">
                    <img src="<?=HOST;?>images/administradores.png" height="25px">
                    <span <?php echo (isset($page[2]) and $page[2] == 'administradores') ? 'style="color:#2E86AB"' : 'style="color:#595758"';?>>Administradores</span>
                </a>
            </li>
        <?php endif; ?>

        <li <?php echo isset($page[2]) and $page[2] == 'sair' ? 'class="active"' : '';?>>
            <a href="<?=HOST;?>sair">
                <img src="<?=HOST;?>images/sair.png" height="25px">
                <span <?php echo (isset($page[2]) and $page[2] == 'sair') ? 'style="color:#2E86AB"' : 'style="color:#595758"';?>>Sair</span>
            </a>
        </li>

    </ul>
</div>
