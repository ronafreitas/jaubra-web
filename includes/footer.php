<!-- Jquery Core Js -->
<script src="<?=PLUGINS;?>jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?=PLUGINS;?>bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js 
<script src="<?=PLUGINS;?>bootstrap-select/js/bootstrap-select.js"></script>-->

<!-- Slimscroll Plugin Js -->
<script src="<?=PLUGINS;?>jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?=PLUGINS;?>node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?=PLUGINS;?>jquery-countto/jquery.countTo.js"></script>

<!--
<script src="<?=JS;?>jquery.mask.js"></script>
-->