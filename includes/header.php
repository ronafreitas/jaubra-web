<?php require_once('defines.php'); ?>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title><?=TITLE;?></title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<!--<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->
<link href="<?=PLUGINS;?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Waves Effect Css -->
<link href="<?=PLUGINS;?>node-waves/waves.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="<?=PLUGINS;?>animate-css/animate.css" rel="stylesheet" />

<!-- Custom Css -->
<link href="<?=CSS;?>style.css" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="<?=CSS;?>themes/all-themes.css" rel="stylesheet" />
<link href="<?=CSS;?>jaubra.css" rel="stylesheet" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">