<?php
/**
* 
*/
class RequestApi {

	public static function POST($url,$data){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$err = curl_error($ch);
		curl_close($ch);
		return array(json_decode($response,true), json_decode($err,true));
		//return json_decode($response,true);
	}


	public static function PUT($url,$data){
		exit;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_PUT, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		//$err = curl_error($ch);
		curl_close($ch);
		return array($response, $err);
		//return json_decode($response,true);
	}


	public static function DELETE($url){
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	    //curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($ch);
	    //$result = json_decode($result);
	    $err = curl_error($ch);
	    curl_close($ch);
	    return array($response, $err);
	}

}