<?php

include_once('Instancia.class.php'); // classe que faz a conexão com o bando de dados

class Base extends Conexao{

    private static $ins;
    private static $banco = 'jaubra'; // nome do banco

    public function __construct() {
        date_default_timezone_set("Brazil/East");
        setlocale(LC_MONETARY, 'pt_BR');
        ini_set('display_errors',0);
        ini_set('display_startup_erros',0);
        error_reporting(0);
    }

    public static function moeda($get_valor) {
        $source = array('.', ','); 
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        return $valor; //retorna o valor formatado para gravar no banco
    }

    public function datas($data){
        $dts = explode('/',$data);
        return $dts[2].'-'. $dts[1].'-'. $dts[0];
        //$date = DateTime::createFromFormat('d/m/Y H:i', $data); return $date->format('Y-m-d');
    }

    public function NormalizaURL($str){
        $str = strtolower(utf8_decode($str)); $i=1;
        $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
        $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
        while($i>0) $str = str_replace('--','-',$str,$i);
        if (substr($str, -1) == '-') $str = substr($str, 0, -1);
        return $str;
    }


    public function calculaIdade($niver=null){
        if($niver == null){
            return false;
            exit;
        }
        $niver = self::dataBrasil($niver);
        $birthDate = $niver;
        $birthDate = explode("/", $birthDate);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") 
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));

        return $age;
    }

    public static function createThumbnail($filepath, $thumbpath, $thumbnail_width, $thumbnail_height, $background=false) {
        list($original_width, $original_height, $original_type) = getimagesize($filepath);
        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_width);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_height);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);

        if ($original_type === 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        } else if ($original_type === 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        } else if ($original_type === 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        } else {
            return false;
        }

        $old_image = $imgcreatefrom($filepath);
        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height); // creates new image, but with a black background

        // figuring out the color for the background
        if(is_array($background) && count($background) === 3) {
          list($red, $green, $blue) = $background;
          $color = imagecolorallocate($new_image, $red, $green, $blue);
          imagefill($new_image, 0, 0, $color);
        // apply transparent background only if is a png image
        } else if($background === 'transparent' && $original_type === 3) {
          imagesavealpha($new_image, TRUE);
          $color = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
          imagefill($new_image, 0, 0, $color);
        }

        imagecopyresampled($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
        $imgt($new_image, $thumbpath);
        return file_exists($thumbpath);
    }

    public static function uploadImage($filesName, $filesTmp, $caminho, $nome){
        if(!file_exists($caminho)){
            mkdir($caminho, 0777, true);
        }

        $target_file = $caminho . basename($filesName);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $nome = $nome.'.'.$imageFileType;
        $fotonome = $caminho.$nome;

        if(move_uploaded_file($filesTmp, $fotonome)){
            return $nome;
        }else{
            return false;
        }
    }

    public static function tempoDeRegistro($data){
        $data_postada = new DateTime($data);
        $data_atual   = new DateTime( date('Y-m-d H:i:s') );

        $interval = date_diff($data_postada,$data_atual);

        $dias = $interval->format('%d');
        $horas = $interval->format('%h');
        $minutos = $interval->format('%i');

        if($dias != 0){
            if($dias > 7){
                return $data_postada->format('d/m/Y');
            }else{
                return 'a '.$dias.' dias atrás';    
            }
        }else{
            if($horas > 0){
                return "a mais de ".$horas." horas";
            }else{
                if($minutos < 5){
                    return "poucos tempo atrás";
                }else{
                    return "a mais de ".$minutos." minutos";
                }
                
            }
        }
    }

    public static function dataBrasil($data){
        // apenas PHP 5.3 >
        //$date = DateTime::createFromFormat('Y-m-d', $data); 
        //return $date->format('d/m/Y');
        return date('d/m/Y', strtotime($data));
    }

    public static function dataBrasilParaTipoDate($data){
        return date('Y-m-d', strtotime($data));
    }

    public function moedaBrasil($valor,$tipo=1){
        if($tipo ==2)   return  number_format($valor, 2, ',', '.');
        else return  number_format($valor, 2, ',', '.');
    }

    public function selected( $value, $selected ){
        return $value==$selected ? ' selected="selected"' : '';
    }
        
    public function checked( $value, $selected ){
        return $value==$selected ? ' checked' : '';
    }
    public function disabled( $value, $selected ){
        return $value==$selected ? ' ' : 'disabled="disabled"';
    }

    public static function geraTimestamp($data) {
        $partes = explode('/', $data);
        return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
    }

    public function interdates($inicio,$fim){
        //date_default_timezone_set('America/Sao_Paulo');

        $time_inicial = self::geraTimestamp($inicio);
        $time_final = self::geraTimestamp($fim);
        $diferenca = $time_final - $time_inicial; 
        $dias = (int)floor( $diferenca / (60 * 60 * 24)); 

        /*
        $inicio = DateTime::createFromFormat('d/m/Y', $inicio);
        $fim = DateTime::createFromFormat('d/m/Y', $fim);
        $intervalo = $inicio->diff($fim);
        return $intervalo->days;*/
        return $dias;
    }

    public static function objectToArray($d) {
        if (is_object($d)){
            $d = get_object_vars($d);
        }
        return is_array($d) ? array_map(__METHOD__, $d) : $d;
    }

    protected static function crypto_rand_secure($min, $max){
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    protected static function getToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[self::crypto_rand_secure(0, $max-1)];
        }

        return $token;
    }

    public static function arrayToJson($a){
        return json_encode($a, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    }



//---------------------  SISTEMA  ---------------------------------------

}