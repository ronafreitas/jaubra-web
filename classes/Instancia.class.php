<?php
/*
$host = 'localhost';
$db   = 'jaubra';
$user = 'jaubra';
$pass = 'sjdj&3m9Mla&01';
$charset = 'utf8mb4';
*/

class Conexao extends PDO {

    private static $instancia;

    protected static function getInstance(){
        if(!isset( self::$instancia )){
            try {
                self::$instancia = new PDO(
                    "mysql:host=localhost;dbname=jaubra",  "root",  "123",
                    //"mysql:host=localhost;dbname=jaubra",  "jaubra",  "sjdj&3m9Mla&01",
                    array(
                        //PDO::ATTR_PERSISTENT => true,
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // DEV -> comentar quando for pra produção
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'")
                    );
            } catch ( Exception $e ) {
                return false;
            }
        }
        return self::$instancia;
    }

    protected static function fecha(){
        self::$instancia = null;
    }
}
