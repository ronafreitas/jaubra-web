<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
if( isset($_GET) ){
	require_once('../includes/defines.php');
	require_once('../classes/RequestApi.class.php');
	$del_end = RequestApi::DELETE(API_URL_WEB.'deletamensagem/'.$_GET['id_mensagem']);
	header('Location: ../mensagens');
}else{
	header('Location: ../mensagens');
}
