<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
if(!isset($_GET['id']) OR $_GET['id'] == ''){
    header('Location: ../administradores');
}
$url = API_URL_WEB.'getadministradores/'.$_GET['id'];
$json = file_get_contents($url);
$admin = json_decode($json);
if(isset($admin->success) AND $admin->success == 2){// consulta bem sucedida mas sem resultado
    header('Location: ../administradores');
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?=CSS;?>bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?=CSS;?>bootstrap-datetimepicker.min.css" rel="stylesheet">
</head>

<body class="theme-teal">
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2> EDITAR ADMINISTRADOR </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    <div style="width:50%">
                        <form method="post" action="../administradores-salvar">
                            <div class="body">
                                <input name="editar" value="1" type="hidden" />
                                <input name="id" value="<?=$admin->id;?>" type="hidden" />
                                <div class="form-group">
                                    <label for="assunto">
                                        Nome
                                    </label>
                                    <input autocomplete="off" value="<?=$admin->nome;?>" class="form-control inputjaubra" required name="nome" type="text" />
                                </div>
                                <div class="form-group">
                                    <label for="assunto">
                                        Email
                                    </label>
                                    <input autocomplete="off" value="<?=$admin->email;?>" class="form-control inputjaubra" required name="email" type="email" />
                                </div>
                                <div class="form-group">
                                    <label for="assunto">
                                        Senha
                                    </label>
                                    <input autocomplete="off" placeholder="para alterar senha insira uma nova ou deixe em branco para manter a atual" class="form-control inputjaubra" name="senha" type="text" />
                                </div>
                                <button class="btn btn-primary">Salvar</button>

                                <div id="load"></div>
                            </div>
                        </form>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

<?php include('../includes/footer.php'); ?>


</body>
</html>