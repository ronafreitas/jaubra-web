<?php
session_start();
ini_set('session.gc_maxlifetime', 80*80);
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}

?>
<!DOCTYPE html>
<html>

<head>
	<?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <!--<link href="<?=CSS;?>bootstrap-datepicker.min.css" rel="stylesheet">-->
    <link href="<?=CSS;?>bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style type="text/css">
        .sweet-alert.show-input input {
            display: block;
            font-weight: bold;
            color: #8e8c8c;
        }
        .inputjaubra{
            padding-left: 5px !important;
            border: 1px solid #2196f3 !important;
        }
        .colj{
            margin-bottom: 0px !important;
        }

        #fileselector {
            margin: 10px; 
        }
        #upload-file-selector {
            display:none;   
        }
        .margin-correction {
            margin-right: 10px;   
        }
        .opcoesadds{
            padding: 12px;
            padding-bottom: 33px;
            border:1px solid #ccc;
            height: 43px;
            width: 50%;
            margin-left: 90px;
        }
    </style>
</head>

<body class="theme-teal">

    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="block-header">
                <h2>
                    DADOS CADASTRAIS - PASSAGEIRO
                    <small>Utilize a interface abaixo para editar os dados cadastrais</small>
                </h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#dados_cadastrais" data-toggle="tab">
                                        <i class="material-icons">face</i> DADOS CADASTRAIS
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#enderecos" data-toggle="tab">
                                        <i class="material-icons">add_location</i> ENDEREÇOS
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#formas_pagamento" data-toggle="tab">
                                        <i class="material-icons">payment</i> FORMAS DE PAGAMENTO
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="dados_cadastrais">

                                    <div>
                                           
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Nome</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Email</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">CPF</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" type="text" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Nacionalidade</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Nascimento</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" type="text" />
                                                </div>
                                            </div>

                                        </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Senha</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" type="text" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Confirmar senha</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj" style="margin-bottom: 50px;">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Foto</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj" >
                                                <div class="form-group">
                                                    <span id="fileselector">
                                                        <label class="btn btn-default" for="upload-file-selector">
                                                            <input id="upload-file-selector" type="file">
                                                           <i class="material-icons">camera_alt</i>selecionar
                                                        </label>
                                                    </span>
                                                </div>

                                                <img style="margin-bottom: 50px;" height="100px;" id="image_upload_preview" src="images/camera.png" alt="foto" />
                                            </div>
                                        </div>

                                    </div>
                                    <div style="margin-top: 20px; padding-left: 95px;">
                                        <span class="btn btn-success btn-lg js-sweetalert waves-effect" data-type="prompt"><b>Salvar</b></span>
                                        <span class="btn btn-danger btn-lg"><b>VOLTAR</b></span>
                                    </div>

                                </div>
                                <!-- fim aba Dados Cadastrais -->




                                <div role="tabpanel" class="tab-pane fade" id="enderecos">
                                    <br/>
                                    <div>
                                           
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">CEP</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="cep" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Logradouro</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" id="logradouro" readonly="" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Bairro</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" id="bairro" readonly="" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Número</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="numero" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj"></div>

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Complem.</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="complemento" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Cidade</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" readonly="" id="cidade" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Estado</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" readonly="" id="estado" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj"></div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span onclick="return adicionarEndereco()" class="btn btn-success btn-lg waves-effect"><b>Adicionar</b></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <br/>
                                            <div id="addEnde"></div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <br/>
                                            <br/>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj"></div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <span class="btn btn-success btn-lg js-sweetalert waves-effect" data-type="prompt"><b>Salvar</b></span>
                                                <span class="btn btn-danger btn-lg"><b>VOLTAR</b></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="clear:both"></div>                                    
                                </div>
                                <!-- fim aba endereços -->




                                <div role="tabpanel" class="tab-pane fade" id="formas_pagamento">
                                    <br/>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj"></div>

                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 colj">
                                            <div class="form-group">
                                                <input type="radio" onclick="return checkPagamento('cartao')" name="pagamento" value="cartao" checked id="cartao" class="with-gap">
                                                <label for="cartao">Cartão de crédito</label>
                                                <input type="radio" onclick="return checkPagamento('dinheiro')" name="pagamento" value="dinheiro" id="dinheiro" class="with-gap">
                                                <label for="dinheiro" class="m-l-20">Dinheiro</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="elempgmt">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Nome no cartão</span>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="nome_cartao" type="text" />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Número do cartão</span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="numero_cartao" type="text" />
                                                </div>
                                            </div>

                                            
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">CVC</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="cvc" type="text" />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Mês vencimento</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="mes_vencimento" type="text" />
                                                </div>
                                            </div>                                      

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Ano vencimento</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="ano_vencimento" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 colj"></div>

                                        </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Bandeira</span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 colj">
                                                <select id="bandeira" class="form-control inputjaubra">
                                                    <option value="visa">Visa</option>
                                                    <option value="master">Master</option>
                                                    <option value="elo">Elo</option>
                                                    <option value="american">American Express</option>
                                                    <option value="diners">Diners Club</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 colj"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                        <br/>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj"></div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                            <span onclick="return adicionarPagamento()" class="btn btn-success btn-lg waves-effect"><b>Adicionar</b></span>
                                        </div>
                                    </div>

                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj"></div>
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                        <br/>
                                        <div id="addPgmto"></div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj"></div>
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                        <br/>
                                        <br/>
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj"></div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                            <span class="btn btn-success btn-lg js-sweetalert waves-effect" data-type="prompt"><b>Salvar</b></span>
                                            <span class="btn btn-danger btn-lg"><b>VOLTAR</b></span>
                                        </div>
                                    </div>


                                    <div style="clear:both"></div>
                                </div>
                                <!-- fim aba formas de pagamento -->
                             
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div>
       
        </div>
    </div>
</section>

<?php include('../includes/footer.php'); ?>
<script src="<?=PLUGINS;?>sweetalert/sweetalert-dev.js"></script>

<script src="<?=PLUGINS;?>momentjs/moment.js"></script>
<script src="<?=JS;?>pt-br.js"></script>
<script src="<?=JS;?>bootstrap-datetimepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=JS;?>jquery.mask.min.js"></script>
<script src="<?=JS;?>CONFIG.js"></script>

<script type="text/javascript">

if(!localStorage.getItem('endereco')){
    localStorage.setItem('endereco',false);
}

var ckdados;

function checkPagamento(tipo){
    if(tipo == 'dinheiro'){
        var x = document.getElementById("elempgmt").querySelectorAll('input,select'),i;
        for(i = 0; i < x.length; i++){
            x[i].disabled = true;
        }
    }else{
        var x = document.getElementById("elempgmt").querySelectorAll('input,select'),i;
        for(i = 0; i < x.length; i++){
            x[i].disabled = false;
        }
    }
}

function adicionarPagamento(){

    var inpts = document.getElementById('elempgmt').querySelectorAll('input,select'),
    cart;
    
    for(var i=0; i<inpts.length; i++){
        if(inpts[i].id == "bandeira"){
            cart = inpts[i].value;
        }
        inpts[i].value='';
    }

    var opcaoendereco = `
    <div class="opcoesadds">
        <span style="float: left;">
            ${cart}
        </span>
        <span style="float: right;">
            <button onclick="return alterarPadraoPaga(this)" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>
            <button onclick="return remove(this)" type="button" class="btn bg-red btn-xs waves-effect">Remover</button>
        </span>
    </div>`;

    var aden = document.getElementById('addPgmto');
    aden.insertAdjacentHTML('afterbegin', opcaoendereco); //beforeend
    localStorage.setItem('endereco',false);

}

function adicionarEndereco(){

    if(localStorage.getItem('endereco') == 'false'){
        alert('necessário preencher CEP');
        return false;
    }

    var inpts = document.getElementById('enderecos').getElementsByTagName('input'),
    logr;
    
    for(var i=0; i<inpts.length; i++){
        if(inpts[i].id == "logradouro"){
            logr = inpts[i].value;
        }
        inpts[i].value='';
    }

    var opcaoendereco = `
    <div class="opcoesadds">
        <span style="float: left;">
            ${logr}
        </span>
        <span style="float: right;">
            <button onclick="return alterarPadraoEnde(this)" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>
            <button onclick="return remove(this)" type="button" class="btn bg-red btn-xs waves-effect">Remover</button>
        </span>
    </div>`;

    var aden = document.getElementById('addEnde');
    aden.insertAdjacentHTML('afterbegin', opcaoendereco); //beforeend
    localStorage.setItem('endereco',false);
}

function remove(ts){
    ts.parentNode.parentNode.remove();
}

function alterarPadraoPaga(ts){

    document.querySelectorAll('#addPgmto').forEach(function(el){
        for(var i=0; i<el.children.length; i++){
            var elemen = el.children[i];
            for(var e=0; e<elemen.children.length; e++){
                var subelem = elemen.children[e].firstElementChild;
                if(subelem){
                    if(subelem.classList.contains('cartaopadrao')){
                        var btnaltpadrao = `<button onclick="return alterarPadraoPaga(this)" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>`;
                        subelem.insertAdjacentHTML('afterend', btnaltpadrao);
                        subelem.remove();
                    }
                }
            }
        }
    });

    var btnpadrao = `<button type="button" class="btn bg-pink btn-xs waves-effect cartaopadrao">Cartão padrão</button>`;
    ts.insertAdjacentHTML('afterend', btnpadrao);
    ts.remove();
}

function alterarPadraoEnde(ts){

    document.querySelectorAll('#addEnde').forEach(function(el){
        for(var i=0; i<el.children.length; i++){
            var elemen = el.children[i];
            for(var e=0; e<elemen.children.length; e++){
                var subelem = elemen.children[e].firstElementChild;
                if(subelem){
                    if(subelem.classList.contains('enderecopadrao')){
                        var btnaltpadrao = `<button onclick="return alterarPadraoEnde(this)" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>`;
                        subelem.insertAdjacentHTML('afterend', btnaltpadrao);
                        subelem.remove();
                    }
                }
            }
        }
    });

    var btnpadrao = `<button type="button" class="btn bg-pink btn-xs waves-effect enderecopadrao">Endereço padrão</button>`;
    ts.insertAdjacentHTML('afterend', btnpadrao);
    ts.remove();
}

function readURL(input){
    if(input.files && input.files[0]){
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_upload_preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function split(val){
    return val.split( /,\s*/ );
}

function extractLast(term) {
    return split(term).pop();
}

$(function(){

    $.getJSON(API_URL+"/getCartoes", function(result){
        $.each(result, function(i, field){
            console.log(i,field);
            //$("div").append(field + " ");
        });
    });


    $("#upload-file-selector").change(function(){
        readURL(this);
    });

    $('.idade').mask('00');

    $(".datamask").on('input', function(){
        $(this).val('');
    });

    $("#cep").autocomplete({
        source: function(request, response){
            $.getJSON(API_URL+"/getEnderecoByCep/"+request.term, function (data) {
                response($.map(data, function (value, key){
                    return {
                        label: value.cep+' - '+value.logradouro,
                        value: value.cep,
                        data: value
                    };
                }));
            });
        },
        select: function(event, ui) {
            //console.log('select',ui.item.data);
            document.getElementById('logradouro').value=ui.item.data.logradouro;
            document.getElementById('bairro').value=ui.item.data.bairro;
            document.getElementById('cidade').value=ui.item.data.cidade;
            document.getElementById('estado').value=ui.item.data.estado;
            localStorage.setItem('endereco',true);
        },
        response: function(event, ui){
            if(ui.content.length == 0){
                //alert('Nada encontrado na sua busca');
                //$('#nomeouemail').addClass('has-danger');
            }else{
               //console.log('response',ui.content);
            }
        },
        search: function() {
            var termo = extractLast( this.value );
            if( termo.length < 1 ){
                return false;
            }
        }
    });

    $('.date').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: "pt-BR",
        keepOpen:false,
    });

    $('.dateend').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: "pt-BR",
        keepOpen:false,
        //minDate: new Date($('.date').val().replace(/-/g, "/")),
       // minDate: new Date($('.date').val()),
    });

    $('.js-sweetalert').on('click', function () {
        showPromptMessage();
    });
});

function showPromptMessage(){
    var d = new Date();
    
    var hora = d.getHours();
    var minuto = d.getMinutes();
    
    if(hora < 10){
        hora = '0'+hora;
    }
    
    if(minuto < 10){
        minuto = '0'+minuto;
    }
    
    var horacompleta = hora+':'+minuto;

    var dia = d.getDate();
    var mes = d.getMonth();
    mes = mes+1;
    if(mes < 9){
        mes ='0'+mes;
    }
    var ano = d.getFullYear();
    var datacompleta = dia+'/'+mes+'/'+ano;
    
    setTimeout(function(){
        $('#dataenvio').val(datacompleta); 
        $('.inputcustom').val(horacompleta); 
    }, 300);

    swal({
        title: "Agendar envio",
        html: true,
        text: "Selecione a data de envio:"+
        "<input style='font-weight: bold;' id='dataenvio' type='text' class='dateenv'><br>Hora do envio: ",                
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        animation: "slide-from-top",
    }, function (inputValue) {

        var data = $('#dataenvio').val();
        var hora = $('.inputcustom').val();

        if(data == ""){
            $('#dataenvio').click();
            return false;
        }

        if(hora == ""){
            $('.inputcustom').focus();
            return false;
        }

        var perfil = document.querySelector('input[name="perfil"]:checked').value;
        var sexo = document.getElementById('sexo').value;
        var titulo = document.getElementById('titulo').value;
        var texto = ckdados;
        var status = document.getElementById('status').value;

        var datacadastroinit = document.getElementById('datacadastroinit').value;
        var datacadastrofim = document.getElementById('datacadastrofim').value;

        var idadeinit = document.getElementById('idadeinit').value;
        var idadefim = document.getElementById('idadefim').value;

        var id_pesquisa = document.getElementById('nome_email_pesquisa').value;
        
        var dados = {
            titulo:titulo,
            texto:texto,
            perfil:perfil,
            sexo:sexo,
            idadeinit:idadeinit,
            idadefim:idadefim,
            status:status,
            id_pesquisa:id_pesquisa,
            datacadastroinit:datacadastroinit,
            datacadastrofim:datacadastrofim,
            data_agenda:data,
            hora_agenda:hora
        };

        $.ajax({
          type: "POST",
          //dataType: 'json',
          url: API_URL+'web/gravarmensagem',
          data: {dados:dados},
          success: function(ret){

            if(ret.success){
                swal({
                    title: "Gravado com sucesso",
                    text: "Sua mensagem foi agendada corretamente",
                    timer: 4000,
                    showConfirmButton: false
                }, function () {
                   location.href = './mensagens';
                });
            }else{
                swal({
                    title: "Ops!",
                    text: "Não foi possível gravar, tente novamente",
                    timer: 4000,
                    showConfirmButton: true
                }, function () {
                    //swal.close();
                   // alert('oookkk');
                });
            }
          },
          error: function(e){
            console.log('e',e);
            swal({
                title: "Ops!",
                text: "Não foi possível gravar, tente novamente",
                timer: 4000,
                showConfirmButton: false
            }, function () {
               // alert('oookkk');
            });
          }

        });

        //swal("Nice!", "You wrote: " + inputValue, "success");

        /*
        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("You need to write something!"); return false
        }
        swal("Nice!", "You wrote: " + inputValue, "success");*/
    });

    $('#dataenvio').on('input', function(){
        $('.dateenv').datetimepicker('show');
        $(this).val('');
    });
   
    $('#dataenvio').on('click', function(){
        $('.dateenv').datetimepicker('show');
        $('.sweet-alert').css({'height':'350px'});
    });

    setTimeout(function(){

        $('.dateenv').datetimepicker({
            format: 'DD/MM/YYYY',
            locale: "pt-BR",
            keepOpen:false,
        });

        $('.inputcustom').focusin(
          function(){  
            $('.sweet-alert').css({'height':'500px'}); 
          }).focusout(  
          function(){  
            $('.sweet-alert').css({'height':'350px'}); 
        });

        $('.inputcustom').datetimepicker({
          format: "HH:mm",
          showClose:true,
          keepOpen:false,
        });

    }, 400);
}
</script>

</body>
</html>