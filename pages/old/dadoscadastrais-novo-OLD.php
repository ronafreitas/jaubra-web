<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
?>
<!DOCTYPE html>
<html>

<head>
	<?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?=CSS;?>bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?=CSS;?>bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

</head>

<body class="theme-teal">

    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="block-header">
                <h2>
                    DADOS CADASTRAIS
                    <small>Utilize a interface abaixo para cadastrar usuários</small>
                </h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#dados_cadastrais" data-toggle="tab">
                                        <i class="material-icons">face</i> DADOS CADASTRAIS
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#enderecos" data-toggle="tab">
                                        <i class="material-icons">add_location</i> ENDEREÇOS
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                
                                <div role="tabpanel" class="tab-pane fade in active" id="dados_cadastrais">
                                    <br/>
                                    <div>
                                        <form method="post" enctype="multipart/form-data" action="gravar-dados-cadastrais" class="formcadastro" id="formcadastro">
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Nome</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" class="form-control inputjaubra" name="cadastro[nome]" id="nome" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Email</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" onblur="return checaemail(this.value)" class="form-control inputjaubra" name="cadastro[email]" id="email" type="email" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">CPF</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" class="form-control inputjaubra" name="cadastro[cpf]" id="cpf" type="text" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Nacionalidade</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" class="form-control inputjaubra" name="cadastro[nacionalidade]" id="nacionalidade" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Nascimento</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" class="form-control inputjaubra" name="cadastro[nascimento]" id="nascimento" type="text" />
                                                </div>
                                            </div>

                                        </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Senha</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" class="form-control inputjaubra" name="cadastro[senha]" id="senha" type="text" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Confirmar senha</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" class="form-control inputjaubra" id="confirmasenha" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj" style="margin-bottom: 50px;">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Foto</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj" >
                                                <div class="form-group">
                                                    <span id="fileselector">
                                                        <label class="btn btn-default" for="imagemsel">
                                                           <input id="imagemsel" name="imagem" type="file">
                                                           <i class="material-icons">camera_alt</i>selecionar
                                                        </label>
                                                    </span>
                                                </div>

                                                <img style="margin-bottom: 50px;" height="100px;" id="image_upload_preview" src="images/camera.png" alt="foto" />
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <!-- fim aba Dados Cadastrais -->




                                <div role="tabpanel" class="tab-pane fade" id="enderecos">
                                    <br/>
                                    <div>
                                           
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">CEP</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="cep" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Logradouro</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" id="logradouro" readonly="" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Bairro</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" id="bairro" readonly="" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Número</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="numero" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj"></div>

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Complem.</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="complemento" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Cidade</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" readonly="" id="cidade" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Estado</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" readonly="" id="estado" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj"></div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span onclick="return adicionarEndereco()" class="btn btn-success btn-lg waves-effect"><b>Adicionar</b></span>
                                            </div>
                                            <div id="newelement"></div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <br/>
                                            <div id="addEnde"></div>
                                        </div>

                                   </form>
                                    </div>

                                    <div style="clear:both"></div>                                    
                                </div>
                                <!-- fim aba endereços -->
                            
                            </div>

                            <span id="salvar" class="btn btn-success btn-lg js-sweetalert waves-effect" onclick="return salvar()"><b>Salvar</b></span>
                            <span class="btn btn-danger btn-lg"><b>VOLTAR</b></span>

                        </div>
                    </div>
                </div>
            </div>

    </div>
</section>

<?php include('../includes/footer.php'); ?>
<script src="<?=PLUGINS;?>sweetalert/sweetalert-dev.js"></script>

<script src="<?=PLUGINS;?>momentjs/moment.js"></script>
<script src="<?=JS;?>pt-br.js"></script>
<script src="<?=JS;?>bootstrap-datetimepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=JS;?>jquery.mask.min.js"></script>
<script src="<?=JS;?>CONFIG.js"></script>

<script type="text/javascript">

localStorage.setItem('endereco',false);

var ckdados;

function validateEmail(email){
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function checaemail(vlr){
    if(vlr != ''){
        if( validateEmail(vlr) ){
            $.ajax({
                type: "GET",
                url: API_URL+'/web/emailexiste/'+vlr,
                success: function(ret){
                    if(ret.length == 1){
                        alert('Email já cadastrado');
                        $('#salvar').hide();
                    }else{
                        $('#salvar').show();
                    }
                },
                error: function(e){

                }
            });
        }else{
            alert('Digite email válido');
            $('#salvar').hide();
        }
    }
}

function criarElemento(name, value){
    //setTimeout(function(){ alert("Hello"); }, 3000);
    var nel = document.createElement("input");
    nel.setAttribute('type', 'hidden');
    nel.setAttribute('value',value);
    nel.setAttribute('name','endereco['+name+'][]');
    document.getElementById('formcadastro').appendChild(nel);

    //var aden = document.getElementById('addEnde');
    //aden.insertAdjacentHTML('afterbegin', opcaoendereco);
}

function adicionarEndereco(){

    if(localStorage.getItem('endereco') == 'false'){
        alert('Encontre um endereço digitando o CEP');
        return false;
    }

    var inpts = document.getElementById('enderecos').getElementsByTagName('input'),logr;

    for(var i=0; i<inpts.length; i++){
        switch(inpts[i].id){
            case 'logradouro':
                logr = inpts[i].value;
                criarElemento(inpts[i].id, inpts[i].value);
                break;
            case 'cep':
                criarElemento(inpts[i].id, inpts[i].value);
                break;
            case 'bairro':
                criarElemento(inpts[i].id, inpts[i].value);
                break;
            case 'numero':
                criarElemento(inpts[i].id, inpts[i].value);
                break;
            case 'complemento':
                criarElemento(inpts[i].id, inpts[i].value);
                break;
            case 'cidade':
                criarElemento(inpts[i].id, inpts[i].value);
                break;
            case 'estado':
                criarElemento(inpts[i].id, inpts[i].value);
                break;
        }
        inpts[i].value='';
    }

    var opcaoendereco = `
    <div class="opcoesadds">
        <span style="float: left;">
            ${logr}
        </span>
        <span style="float: right;">
            <button onclick="return alterarPadraoEnde(this)" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>
            <button onclick="return remove(this)" type="button" class="btn bg-red btn-xs waves-effect">Remover</button>
        </span>
    </div>`;

    var aden = document.getElementById('addEnde');
    aden.insertAdjacentHTML('afterbegin', opcaoendereco);
    localStorage.setItem('endereco',false);
}

function remove(ts){
    ts.parentNode.parentNode.remove();
}

function alterarPadraoEnde(ts){

    document.querySelectorAll('#addEnde').forEach(function(el){
        for(var i=0; i<el.children.length; i++){
            var elemen = el.children[i];
            for(var e=0; e<elemen.children.length; e++){
                var subelem = elemen.children[e].firstElementChild;
                if(subelem){
                    if(subelem.classList.contains('enderecopadrao')){
                        var btnaltpadrao = `<button onclick="return alterarPadraoEnde(this)" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>`;
                        subelem.insertAdjacentHTML('afterend', btnaltpadrao);
                        subelem.remove();
                    }
                }
            }
        }
    });

    var btnpadrao = `<button type="button" class="btn bg-pink btn-xs waves-effect enderecopadrao">Endereço padrão</button>`;
    ts.insertAdjacentHTML('afterend', btnpadrao);
    ts.remove();
}

function readURL(input){
    if(input.files && input.files[0]){
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_upload_preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function split(val){
    return val.split( /,\s*/ );
}

function extractLast(term) {
    return split(term).pop();
}

$(function(){

    $("#imagemsel").change(function(){
        readURL(this);
    });

    $('#cpf').mask('000.000.000-00');
    $('#nascimento').mask('00/00/0000');

    $(".datamask").on('input', function(){
        $(this).val('');
    });

    $("#cep").autocomplete({
        source: function(request, response){
            $.getJSON(API_URL+"/getEnderecoByCep/"+request.term, function (data) {
                response($.map(data, function (value, key){
                    return {
                        label: value.cep+' - '+value.logradouro,
                        value: value.cep,
                        data: value
                    };
                }));
            });
        },
        select: function(event, ui){
            document.getElementById('logradouro').value=ui.item.data.logradouro;
            document.getElementById('bairro').value=ui.item.data.bairro;
            document.getElementById('cidade').value=ui.item.data.cidade;
            document.getElementById('estado').value=ui.item.data.estado;
            localStorage.setItem('endereco',true);
        },
        response: function(event, ui){
            if(ui.content.length == 0){
                //alert('Nada encontrado na sua busca');
                //$('#nomeouemail').addClass('has-danger');
            }else{
               //console.log('response',ui.content);
            }
        },
        search: function() {
            var termo = extractLast( this.value );
            if( termo.length < 1 ){
                return false;
            }
        }
    });

    $('.date').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: "pt-BR",
        keepOpen:false,
    });

    $('.dateend').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: "pt-BR",
        keepOpen:false,
        // minDate: new Date($('.date').val().replace(/-/g, "/")),
        // minDate: new Date($('.date').val()),
    });

});

var continua=true;
function obrigatorio(elem){
    if(elem.value == ""){
        elem.classList.remove('inputjaubra');
        elem.classList.add('inputjaubra_erro');
        continua=false;
    }else{
        elem.classList.remove('inputjaubra_erro');
        elem.classList.add('inputjaubra');
        continua=true;
    }
}

function salvar(){
    //var camposobrig = [];
    var camposobrig = ['nome','email', 'cpf','senha', 'confirmasenha'];

    var inpts = document.getElementsByTagName('input');
    var in_ids = [];
    var in_values = [];

    for(var i=0; i<inpts.length; i++){
        var id = inpts[i].id;
        if(id != ""){
            if(id != "imagemsel"){
                for(var o in camposobrig){
                    if(inpts[i].id == camposobrig[o]){
                        obrigatorio(inpts[i]);
                    }
                }
            }
        }
    }

    if(document.getElementById('senha').value != document.getElementById('confirmasenha').value){
        alert('Confirme a senha correta');
        return false;
    }

    for(var i=0; i<inpts.length; i++){
        var id = inpts[i].id;
        if(id != ""){
            if(id != 'confirmasenha'){
                if(id == "imagemsel"){
                    in_ids.push(id);
                    in_values.push({id:id, valor: inpts[i].value});
                }else{
                    in_ids.push(id);
                    in_values.push(inpts[i].value);
                }
            }
        }
    }

    var ds=[];
    for(var d in in_ids){
        ds[in_ids[d]] = in_values[d];
    }

    if(continua){
        document.getElementById("formcadastro").submit();
    }else{
        $('a[href="#dados_cadastrais"]').click();
        alert('Os campos em vermelho são obrigatórios');
    }

}

</script>

</body>
</html>