<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
//require_once('../classes/Encrpt.class.php');
//$enc = new Encpt();

if(isset($_GET) and count($_GET) > 0){
    $param = $_GET;

    $date1 = explode('/', $param['df']);
    $di    = $date1[2].'-'.$date1[1].'-'.$date1[0];
    $date1 = explode('/', $param['di']);
    $df    = $date1[2].'-'.$date1[1].'-'.$date1[0];

    $p   = base64_encode($param['perfil'].'/'.$param['situacao'].'/'.$di.'/'.$df);
    $url = API_URL_WEB.'mensagens/'.$p;
}else{
    $url = API_URL_WEB.'mensagens/0';
}

$json = file_get_contents($url);
$mensagens = json_decode($json);
//echo "<br/><pre>"; print_r($mensagens); exit;
?>
<!DOCTYPE html>
<html>
<head>
	<?php 
        include('../includes/header.php'); 
        require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?=PLUGINS;?>jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?=CSS;?>bootstrap-datepicker.min.css" rel="stylesheet">
    <style type="text/css">
        .well{
            box-shadow: 3px 2px 0px 1px #c3c7dc;
            background-color: #d7d9e3;
        }
        .titunmemp{
            font-size: 20px;
            border-radius: 5px;
            color: #fff;
            padding: 6px;
            background-color: #a1a7c7;
        }
    </style>
</head>

<body class="theme-teal">

    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    ENVIO DE MENSAGENS
                    <small>Utilize a interface abaixo para gerenciar o envio de mensagens aos clientes e motoristas</small>
                </h2>
            </div>
            
            <div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                              CONSULTAR MENSAGENS ENVIADAS 
                            </h2>
                        </div>

                        <div style="padding: 20px;">
                            <div class="form-group">
                                Selecione o perfil &nbsp;&nbsp;&nbsp; 
                                <input <?php echo (isset($_GET['perfil']) and $_GET['perfil'] == 'motorista') ? 'checked=""' : '';?> type="radio" value="motorista" name="perfil" id="motorista" class="with-gap">
                                <label for="motorista">Motorista</label>

                                <input <?php echo (isset($_GET['perfil']) and $_GET['perfil'] == 'passageiro') ? 'checked=""' : '';?> type="radio" value="passageiro" name="perfil" id="passageiro" class="with-gap">
                                <label for="passageiro" class="m-l-20">Passageiro</label>

                                <input <?php echo (isset($_GET['perfil']) and $_GET['perfil'] == 'todos') ? 'checked=""' : '';?> value="todos" type="radio" name="perfil" id="todos" class="with-gap">
                                <label for="todos" class="m-l-20">Todos</label>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <div style="width: 50%">
                                    <label for="sel1">Situação</label>
                                    <select class="form-control" id="sel1">
                                        <option value="enviado">Enviado</option>
                                        <option value="agendado">Agendado</option>
                                    </select>
                                </div> 
                            </div>
                            
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                <label for="sel1">Data de envio</label><br/>
                                <div style="width: 40%; float: left;">
                                    <div class="input-group date">
                                        <input id="data_inicio" value="<?php echo (isset($_GET['di'])) ? $_GET['di'] : '';?>" type="text" placeholder="início" class="form-control" style="padding-left: 5px;border-left: 1px solid #cccccc; border-top: 1px solid #cccccc; border-bottom:  1px solid #cccccc;">
                                        <span class="input-group-addon" style="border-right: 1px solid #cccccc; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                                            <i class="glyphicon glyphicon-th"></i>
                                        </span>
                                    </div>
                                </div>
                                <span style="float: left; padding: 5px">a</span>
                                <div style="width: 40%; float: left; ">
                                    <div class="input-group date">
                                        <input id="data_fim" value="<?php echo (isset($_GET['df'])) ? $_GET['df'] : '';?>" type="text" class="form-control" placeholder="fim" style="padding-left: 5px;border-left: 1px solid #cccccc; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                                        <span class="input-group-addon" style="border-right: 1px solid #cccccc; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                                            <i class="glyphicon glyphicon-th"></i>
                                        </span>
                                    </div>
                                </div>
                                <span onclick="return buscar()" class="btn btn-default pull-right">
                                    buscar
                                </span>
                            </div>
                            <br/>
                            <br/>
                        </div>
                        
                        <div class="body">
                            <div>
                            <!--<div class="table-responsive">-->
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Data de envio</th>
                                            <th>Título mensagem</th>
                                            <th>Perfil</th>
                                            <th>Situação</th>
                                            <th>Operação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($mensagens as $mens):?>
                                            <div id="<?=$mens->id;?>">
                                                <input type="hidden" value="<?=$mens->titulo;?>" id="titulo"><input type="hidden" value="<?=$mens->texto;?>" id="texto"><input type="hidden" value="<?=$mens->sexo;?>" id="sexo"><input type="hidden" value="<?=$mens->perfil;?>" id="perfil"><input type="hidden" value="<?=$mens->status;?>" id="status"><input type="hidden" value="<?=$mens->idadeinit;?>" id="idadeinit"><input type="hidden" value="<?=$mens->idadefim;?>" id="idadefim"><input type="hidden" value="<?=$mens->datacadastroinit;?>" id="datacadastroinit"><input type="hidden" value="<?=$mens->datacadastrofim;?>" id="datacadastrofim"><input type="hidden" value="<?=$mens->data_agenda;?>" id="data_agenda"><input type="hidden" value="<?=$mens->hora_agenda;?>" id="hora_agenda">
                                            </div>
                                            <tr>
                                                <td><?=$mens->data_agenda;?></td>
                                                <td><?=$mens->titulo;?></td>
                                                <td><?=$mens->perfil;?></td>
                                                <td>Agendada</td>
                                                <td>
                                                    <a onclick="return visualizarmensagem(<?=$mens->id;?>)" title="visualizar mensagem" style="margin-right: 20px" id="modal-705218" href="#modal-container-705218" data-toggle="modal"><img src="<?=HOST;?>images/detalhes-mensagem.png"></a>
                                                    <!-- href="enviar-agora/<?=$mens->id;?>" -->
                                                    <a title="enviar agora" href="#" style="margin-right: 10px"><img src="<?=HOST;?>images/enviar-agora.png"></a>
                                                    <span title="apagar mensagem" style="cursor: pointer;" onclick="return apagar(<?=$mens->id;?>)"><img src="<?=HOST;?>images/excluir.png"></span>
                                                </td>
                                            </tr>    
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <br/>
                            <a href="agendar-mensagem" class="btn btn-success btn-lg"><b>NOVA MENSAGEM</b></a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>

            
    <div class="modal fade" id="modal-container-705218" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel">
                        Detalhes da mensagem
                    </h5> 
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div id="element" class="modal-body"></div>
                <div class="modal-footer">
                     
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Ok
                    </button>
                </div>
            </div>
        </div>
    </div>

  

<?php include('../includes/footer.php'); ?>
<script src="<?=PLUGINS;?>sweetalert/sweetalert.min.js"></script>

<script src="<?=PLUGINS;?>jquery-datatable/jquery.dataTables.js"></script>
<script src="<?=PLUGINS;?>jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?=JS;?>bootstrap-datepicker.min.js"></script>
<script src="locales/bootstrap-datepicker.pt-BR.min.js"></script>

<script type="text/javascript">
    function buscar(){
        var perfil      = (document.querySelector('input[name="perfil"]:checked')) ? document.querySelector('input[name="perfil"]:checked').value : '';
        var situacao    = document.getElementById('sel1').value;
        var data_inicio = document.getElementById('data_inicio').value;
        var data_fim    = document.getElementById('data_fim').value;

        if(perfil == ""){
            alert('Necessário selecionar perfil');
            return false;
        }

        if(data_inicio == "" || data_fim == ""){
            alert('Necessário selecionar as datas de envio');
            return false;
        }

        var strData = data_inicio;
        var endData = data_fim;
        var partesData1 = strData.split("/");
        var partesData2 = endData.split("/");
        var data1 = new Date(partesData1[2], partesData1[1] - 1, partesData1[0]);
        var data2 = new Date(partesData2[2], partesData2[1] - 1, partesData2[0]);
        if(data1 > data2){
           alert("A data inicial de envio deve ser menor ou igual que a data final");
           return false;
        }

        location.href = '?perfil='+perfil+'&situacao='+situacao+'&di='+data_inicio+'&df='+data_fim;
    }

    function visualizarmensagem(id){

        var inpts = document.getElementById(id).getElementsByTagName('input');
        var html='';
        for(var i=0; i<inpts.length; i++){
            var campo;
            switch(inpts[i].id){

                case 'titulo':
                    campo = 'Título';
                    break;
                case 'texto':
                    campo = 'Mensagem';
                    break;
                case 'perfil':
                    campo = 'Perfil';
                    break;
                case 'sexo':
                    campo = 'Sexo';
                    break;
                case 'status':
                    campo = 'Status';
                    break;
                case 'idadeinit':
                    campo = 'Idade (início)';
                    break;
                case 'idadefim':
                    campo = 'Idade (fim)';
                    break;
                case 'datacadastroinit':
                    campo = 'Data de cadastro (início)';
                    break;
                case 'datacadastrofim':
                    campo = 'Data de cadastro (fim)';
                    break;
                case 'data_agenda':
                    campo = 'Data agendada';
                    break;
                case 'hora_agenda':
                    campo = 'Hora agendada';
                    break;
            }

            html +='<div class="form-group">';
                html +='<label id="key">'+campo+'</label><br/>';
                html +='<span id="value">'+inpts[i].value+'</span>';
            html +='</div>';
        }
        document.getElementById('element').innerHTML = html;
    }

    function apagar(id){
        var r = confirm("Deseja mesmo apagar?");
        if (r == true) {
            location.href = 'apagar-mensagem/'+id;
        }
    }

    $(function () {
        $('.input-group.date').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true,
            orientation: "bottom right"
        });

        $('.js-basic-example').DataTable({
            responsive: true,
            "lengthChange": false,
            "searching": false,
            language: {
                //processing:     "Traitement en cours...",
                //search:         "Rechercher&nbsp;:",
                //lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                info:           "Exibindo _START_ de _END_",
                //infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                //infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                //infoPostFix:    "",
                //loadingRecords: "Chargement en cours...",
                zeroRecords:    "Nenhum resultado encontrado",
                emptyTable:     "Não existem dados para serem exibidos",
                paginate: {
                    first:      "Início",
                    previous:   "Anterior",
                    next:       "Próximo",
                    last:       "Fim"
                },
                aria: {
                    //sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    //sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });
    });

</script>
</body>
</html>