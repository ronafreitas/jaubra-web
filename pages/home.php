<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
/*
Array
(
    [logado] => 1
    [jadm] => 1
    [per] => 0
)

*/
?>
<!DOCTYPE html>
<html>

<head>
	<?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <style type="text/css">
        
        .well{
            box-shadow: 3px 2px 0px 1px #c3c7dc;
            background-color: #d7d9e3;
        }

        .titunmemp{
            font-size: 20px;
            border-radius: 5px;
            color: #fff;
            padding: 6px;
            background-color: #a1a7c7;
        }
    </style>
</head>

<body class="theme-teal">

    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <span style="font-size: 18px;"> Home</span>
            </div>
            
            <div class="row clearfix">
                
            </div>
        </div>
    </section>

	<?php include('../includes/footer.php'); ?>
    <script src="<?=PLUGINS;?>sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">

        function enc(str) {
            var encoded = "";
            for (i=0; i<str.length;i++) {
                var a = str.charCodeAt(i);
                var b = a ^ 123;    // bitwise XOR with any number, e.g. 123
                encoded = encoded+String.fromCharCode(b);
            }
            return encoded;
        }

        function linkChat(url, path, id){
            //var cId = enc(id);
            var cId = encodeURIComponent(btoa(id));
            //var b = atob(decodeURIComponent(cId));
            var urlfinal = url+'register/'+path+'/'+cId;
            console.log(urlfinal);
            window.open(urlfinal,'_blank');
        }

        function sendPost(path, params, method){
            method = method || "post";
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", path);
            for(var key in params){
                if(params.hasOwnProperty(key)){
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", key);
                    hiddenField.setAttribute("value", params[key]);
                    form.appendChild(hiddenField);
                }
            }
            document.body.appendChild(form);
            form.submit();
        }

        function apagarFluxo(idFluxo, nomeVaga){
            swal({
                title: "Deseja mesmo remover o fluxo '"+nomeVaga+"'?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: true
            }, function () {
                location.href = '../deletar-fluxo/'+idFluxo;
            });
        }

        function apagarEmpresa(path, nomeEmpresa){
            swal({
                title: "Deseja mesmo remover essa empresa: '"+nomeEmpresa+"'?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: true
            }, function () {
                location.href = '../deletar-empresa/'+path;
            });
        }

        function criarEmpresa(){
            swal({
                title: "Nova empresa/instituição",
                text: "Insira o nome:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Escreva o nome da empresa/instituição"
            }, function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue.trim() === "") {
                    swal.showInputError("Esse campo não pode ficar vazio."); return false
                }
                sendPost('../criar-instancia', {nome: inputValue.trim()});
            });
        }

    </script>
</body>
</html>