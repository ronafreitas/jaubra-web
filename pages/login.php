﻿<?php include('../includes/defines.php'); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?=TITLE;?> - Acessar</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?=PLUGINS;?>bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?=PLUGINS;?>node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?=PLUGINS;?>animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?=CSS;?>style.css" rel="stylesheet">
    <style type="text/css">
        .input-group {
            width: 100%;
        }
    </style>
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <center>
            <img src="../images/logotipo.png" width="150">
            </center>
            
            <?php 
            if( isset($_GET['resp']) and $_GET['resp'] == 'errorlogin' ){
                echo '<center style="color: red; padding:10px; font-size: 18px; background-color:#fff; margin-top:10px;">Acesso negado, verifique login/senha.</center>';
            }
            if( isset($_GET['resp']) and $_GET['resp'] == 'exist' ){
                echo '<center style="color: red; padding:10px; font-size: 18px; background-color:#fff; margin-top:10px;">Email já registrado.</center>';
            }
            if( isset($_GET['resp']) and $_GET['resp'] == 'emailexists' ){
                echo '<center style="color: red; padding:10px; font-size: 18px; background-color:#fff; margin-top:10px;">Email já registrado.</center>';
            } 
            ?>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="post" action="<?=HOST;?>logar">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" autocomplete="off" class="form-control" name="email_login" placeholder="Email" required>
                        </div>
                        <?php 
                        if( isset($_GET['resp']) and $_GET['resp'] == 'emailerror' ){
                            echo '<label id="email-error" class="error" for="email">Informe um email válido.</label>';
                        } 
                        ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" autocomplete="off" class="form-control" name="senha_login" placeholder="Senha" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-block bg-default hire_azul_escuro waves-effect" type="submit">Acessar</button>
                        </div>
                    </div>
                   <div class="row">
                        <div class="col-xs-12">
                            <a class="btn btn-block btn-link" href="<?=HOST;?>cadastrar/">Deseja cadastrar?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?=PLUGINS;?>jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?=PLUGINS;?>bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?=PLUGINS;?>node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?=PLUGINS;?>jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?=JS;?>admin.js"></script>
    <script src="<?=JS;?>sign-in.js"></script>
</body>

</html>