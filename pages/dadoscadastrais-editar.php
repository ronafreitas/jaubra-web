<?php
session_start();
ini_set('session.gc_maxlifetime', 80*80);
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}

$url = API_URL_WEB.'usuario/'.$_GET['iduser'];
$json = file_get_contents($url);
$usuario = json_decode($json);

$nacionalidade = isset($usuario[0]->nacionalidade) ? $usuario[0]->nacionalidade :'';
$nascimento = isset($usuario[0]->nascimento) ? $usuario[0]->nascimento :'';

?>
<!DOCTYPE html>
<html>

<head>
    <?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?=CSS;?>bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style type="text/css">
        .sweet-alert.show-input input {
            display: block;
            font-weight: bold;
            color: #8e8c8c;
        }
        .inputjaubra{
            padding-left: 5px !important;
            border: 1px solid #2196f3 !important;
        }
        .inputjaubra_erro{
            padding-left: 5px !important;
            border: 1px solid red !important;
        }
        .colj{
            margin-bottom: 0px !important;
        }

        #fileselector {
            margin: 10px; 
        }
        #imagemsel {
            display:none;   
        }
        .margin-correction {
            margin-right: 10px;   
        }
        .opcoesadds{
            padding: 12px;
            padding-bottom: 33px;
            border:1px solid #ccc;
            height: 43px;
            width: 50%;
            margin-left: 90px;
        }
        .formdados{

        }
    </style>
</head>

<body class="theme-teal">

    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">

            <div class="block-header">
                <h2>
                    DADOS CADASTRAIS
                    <small>Utilize a interface abaixo para cadastrar usuários</small>
                </h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#dados_cadastrais" data-toggle="tab">
                                        <i class="material-icons">face</i> DADOS CADASTRAIS
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#enderecos" data-toggle="tab">
                                        <i class="material-icons">add_location</i> ENDEREÇOS
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                
                                <div role="tabpanel" class="tab-pane fade in active" id="dados_cadastrais">
                                    <br/>
                                    <div>
                                        <form method="POST" enctype="multipart/form-data" action="../gravar-dados-cadastrais" class="formcadastro" id="formcadastro">

                                        <input type="hidden" name="cadastro[id]" value="<?=$usuario[0]->id;?>">
                                        <input type="hidden" name="editar" value="1">
                                        <input type="hidden" name="cadastro[foto_atual]" value="<?=$usuario[0]->foto;?>">
                                        <input type="hidden" id="idusuario" name="endereco[id_usuario]" value="<?=$_GET['iduser'];?>">

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Nome</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" value="<?=$usuario[0]->nome;?>" class="form-control inputjaubra" name="cadastro[nome]" id="nome" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Email</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" value="<?=$usuario[0]->email;?>" class="form-control inputjaubra" name="cadastro[email]" id="email" type="email" />
                                                    <input value="<?=$usuario[0]->email;?>" id="email_atual" type="hidden" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">CPF</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" value="<?=$usuario[0]->cpf;?>" class="form-control inputjaubra" name="cadastro[cpf]" id="cpf" type="text" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Nacionalidade</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" value="<?=$nacionalidade;?>" class="form-control inputjaubra" name="cadastro[nacionalidade]" id="nacionalidade" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span class="txttitjaubra">Nascimento</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" value="<?=$nascimento;?>" class="form-control inputjaubra" name="cadastro[nascimento]" id="nascimento" type="text" />
                                                </div>
                                            </div>

                                        </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Senha</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" value="<?=$usuario[0]->senha;?>" class="form-control inputjaubra" name="cadastro[senha]" id="senha" type="text" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Confirmar senha</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input autocomplete="off" value="<?=$usuario[0]->senha;?>" class="form-control inputjaubra" id="confirmasenha" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj" style="margin-bottom: 50px;">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Foto</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj" >
                                                <div class="form-group">
                                                    <span id="fileselector">
                                                        <label class="btn btn-default" for="imagemsel">
                                                           <input id="imagemsel" name="imagem" type="file">
                                                           <i class="material-icons">camera_alt</i>selecionar
                                                        </label>
                                                    </span>
                                                </div>
                                                <?php
                                                if($usuario[0]->foto != ""){
                                                    $foto = HOST.'uploads/profiles/'.$usuario[0]->foto;
                                                }else{
                                                    $foto = '../images/camera.png';
                                                }
                                                ?>
                                                <img style="margin-bottom: 50px;" height="100px;" id="image_upload_preview" src="<?=$foto;?>" />
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <!-- fim aba Dados Cadastrais -->




                                <div role="tabpanel" class="tab-pane fade" id="enderecos">
                                    <br/>
                                    <div>
                                           
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">CEP</span>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="cep" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Logradouro</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" id="logradouro" readonly="" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Bairro</span>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" id="bairro" readonly="" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Número</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="numero" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj"></div>

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Complem.</span>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 colj">
                                                <div class="form-group">
                                                    <input class="form-control inputjaubra" id="complemento" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Cidade</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" readonly="" id="cidade" type="text" />
                                                </div>
                                            </div>

                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj">
                                                <span class="txttitjaubra">Estado</span>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 colj">
                                                <div class="form-group">
                                                    <input placeholder="preencha o CEP" class="form-control inputjaubra" readonly="" id="estado" type="text" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 colj"></div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 colj">
                                                <span onclick="return adicionarEndereco()" class="btn btn-success btn-lg waves-effect"><b>Adicionar</b></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 colj">
                                            <br/>
                                            <div id="addEnde"></div>
                                        </div>

                                   </form>
                                    </div>

                                    <div style="clear:both"></div>                                    
                                </div>
                                <!-- fim aba endereços -->
                            
                            </div>

                            <span class="btn btn-success btn-lg js-sweetalert waves-effect" onclick="return salvar()"><b>Editar</b></span>
                            <a class="btn btn-danger btn-lg" href="../dadoscadastrais"><b>VOLTAR</b></a>

                        </div>
                    </div>
                </div>
            </div>

    </div>
</section>

<?php include('../includes/footer.php'); ?>
<script src="<?=PLUGINS;?>sweetalert/sweetalert-dev.js"></script>

<script src="<?=PLUGINS;?>momentjs/moment.js"></script>
<script src="<?=JS;?>pt-br.js"></script>
<script src="<?=JS;?>bootstrap-datetimepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=JS;?>jquery.mask.min.js"></script>
<script src="<?=JS;?>CONFIG.js"></script>

<script type="text/javascript">

localStorage.setItem('endereco',false);

var ckdados;

function readURL(input){
    if(input.files && input.files[0]){
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image_upload_preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function split(val){
    return val.split( /,\s*/ );
}

function extractLast(term) {
    return split(term).pop();
}

var contemail=true;

function adicionarEndereco(){
    if(localStorage.getItem('endereco') == 'false'){
        alert('Encontre um endereço digitando o CEP');
        return false;
    }
    endereco_element();
    localStorage.setItem('endereco',false);
}

function removeElem(ts, idelem){
    contagemelemend--;
    ts.parentNode.parentNode.remove();
    document.getElementById(idelem).remove();
}

function alterarPadraoEnde(ts,idelem){
    document.querySelectorAll('.padroes').forEach(function(elm){
        elm.value = '0';
        var idele = idelem;
        idele = idele.toString();
        if(elm.id == 'padrao_'+idele){
            elm.value = '1';
        }
    });
    document.querySelectorAll('#addEnde').forEach(function(el){
        for(var i=0; i<el.children.length; i++){
            var elemen = el.children[i];
            for(var e=0; e<elemen.children.length; e++){
                var subelem = elemen.children[e].firstElementChild;
                if(subelem){
                    if(subelem.classList.contains('enderecopadrao')){
                        var btnaltpadrao = `<button onclick="return alterarPadraoEnde(this, ${idelem})" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>`;
                        subelem.insertAdjacentHTML('afterend', btnaltpadrao);
                        subelem.remove();
                    }
                }
            }
        }
    });
    var btnpadrao = `<button type="button" class="btn bg-pink btn-xs waves-effect enderecopadrao">Endereço padrão</button>`;
    ts.insertAdjacentHTML('afterend', btnpadrao);
    ts.remove();
}
var contagemelemend=0;

function criarElemento(name, value, ideleme,div){
    var nel = document.createElement("input");
    nel.setAttribute('type', 'text');
    nel.setAttribute('value',value);
    if(name == 'padrao'){
        //var rnd = Math.floor((Math.random() * 1234) + 10);
        nel.setAttribute('id',ideleme);
    }
    nel.setAttribute('name','endereco['+name+'][]');
    //div.appendChild(nel);
}

function endereco_element(valores=null){
    if(contagemelemend > 2){
        alert('Só é possível adicionar 3 endereços');
        return false;
    }
    var logr;
    if(valores){
        var inpts = valores;
        for(var i in inpts){
            var htmlelem='';
            var inpelem = inpts[i];
            for(var e in inpelem){
                var valor = inpelem[e];
                
                switch(e){
                    case 'logradouro':
                        htmlelem += `<input type='hidden' value='${valor}' name="endereco[logradouro][]" />`;
                        break;
                    case 'cep':
                        htmlelem += `<input type='hidden' value='${valor}' name="endereco[cep][]" />`;
                        break;
                    case 'bairro':
                        htmlelem += `<input type='hidden' value='${valor}' name="endereco[bairro][]" />`;
                        break;
                    case 'numero':
                        htmlelem += `<input type='hidden' value='${valor}' name="endereco[numero][]" />`;
                        break;
                    case 'complemento':
                        htmlelem += `<input type='hidden' value='${valor}' name="endereco[complemento][]" />`;
                        break;
                    case 'cidade':
                        htmlelem += `<input type='hidden' value='${valor}' name="endereco[cidade][]" >`;
                        break;
                    case 'estado':
                        htmlelem += `<input type='hidden' value='${valor}' name="endereco[estado][]" />`;
                        break;
                    case 'padrao':
                        var padrao = valor;
                        break;
                }
            }
            
            var idrdnel = Math.floor((Math.random() * 1234) + 10);
            htmlelem += `<input type='hidden' class="padroes" value='${padrao}' id="padrao_${idrdnel}" name="endereco[padrao][]" />`;
            var opcendinpt = `
                <div id="${idrdnel}">
                    ${htmlelem}
                </div>
            `;
            var adenform = document.getElementById('formcadastro');
            adenform.insertAdjacentHTML('afterbegin', opcendinpt);

            var opcaoendereco = `
            <div id="${idrdnel}" class="opcoesadds">
                <span style="float: left;"> 
                    ${inpts[i].logradouro}
                </span>`;
            if(padrao == '1'){
                opcaoendereco += `
                <span style="float: right;">
                    <button type="button" class="btn bg-pink btn-xs waves-effect enderecopadrao">Endereço padrão</button>
                    <button onclick="return removeElem(this,${idrdnel})" type="button" class="btn bg-red btn-xs waves-effect">Remover</button>
                </span>`;
            }else{
                opcaoendereco += `
                <span style="float: right;">
                    <button onclick="return alterarPadraoEnde(this,${idrdnel})" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>
                    <button onclick="return removeElem(this,${idrdnel})" type="button" class="btn bg-red btn-xs waves-effect">Remover</button>
                </span>`;
            }
            opcaoendereco += `</div>`;
            var adend = document.getElementById('addEnde');
            adend.insertAdjacentHTML('afterbegin', opcaoendereco);
            contagemelemend++;
        }

    }else{
        var inpts = document.getElementById('enderecos').getElementsByTagName('input');
        var htmlelem='';
        for(var i=0; i<inpts.length; i++){
            switch(inpts[i].id){
                case 'logradouro':
                    logr = inpts[i].value;
                    //criarElemento(inpts[i].id, inpts[i].value, idrdnel, div);
                    htmlelem += `<input type='hidden' value='${inpts[i].value}' name="endereco[logradouro][]" />`;
                    break;
                case 'cep':
                    htmlelem += `<input type='hidden' value='${inpts[i].value}' name="endereco[cep][]" />`;
                    break;
                case 'bairro':
                    htmlelem += `<input type='hidden' value='${inpts[i].value}' name="endereco[bairro][]" />`;
                    break;
                case 'numero':
                    htmlelem += `<input type='hidden' value='${inpts[i].value}' name="endereco[numero][]" />`;
                    break;
                case 'complemento':
                    htmlelem += `<input type='hidden' value='${inpts[i].value}' name="endereco[complemento][]" />`;
                    break;
                case 'cidade':
                    htmlelem += `<input type='hidden' value='${inpts[i].value}' name="endereco[cidade][]" />`;
                    break;
                case 'estado':
                    htmlelem += `<input type='hidden' value='${inpts[i].value}' name="endereco[estado][]" />`;
                    break;
            }
            inpts[i].value='';
            
        }
        var idrdnel = Math.floor((Math.random() * 1234) + 10);
        htmlelem += `<input type='hidden' value='0' class="padroes" id="padrao_${idrdnel}" name="endereco[padrao][]" />`;
        var opcendinpt = `
            <div id="${idrdnel}">
                ${htmlelem}
            </div>
        `;
        var adenform = document.getElementById('formcadastro');
        adenform.insertAdjacentHTML('afterbegin', opcendinpt);
        var opcaoendereco = `
        <div id="${idrdnel}" class="opcoesadds">
            <span style="float: left;"> 
                ${logr}
            </span>
            <span style="float: right;">
                <button onclick="return alterarPadraoEnde(this,${idrdnel})" type="button" class="btn bg-cyan btn-xs waves-effect">Alterar padrão</button>
                <button onclick="return removeElem(this,${idrdnel})" type="button" class="btn bg-red btn-xs waves-effect">Remover</button>
            </span>
        </div>`;
        var adend = document.getElementById('addEnde');
        adend.insertAdjacentHTML('afterbegin', opcaoendereco);
        contagemelemend++;
    }
    
}

$(function(){

    $.getJSON(API_URL+"/web/enderecosuser/"+document.getElementById('idusuario').value, function (data) {
        endereco_element(data);
    });

    $("#imagemsel").change(function(){
        readURL(this);
    });

    if($('#cep').val()){
        localStorage.setItem('endereco',true);
    }
    $('#nascimento').mask('00/00/0000');

    $('#cpf').mask('000.000.000-00');

    $("#email").on('input', function(){
        contemail=false;
        console.log('aaaaa');
    });

    $("#cep").autocomplete({
        source: function(request, response){
            $.getJSON(API_URL+"/getEnderecoByCep/"+request.term, function (data) {
                response($.map(data, function (value, key){
                    return {
                        label: value.cep+' - '+value.logradouro,
                        value: value.cep,
                        data: value
                    };
                }));
            });
        },
        select: function(event, ui){
            document.getElementById('logradouro').value=ui.item.data.logradouro;
            document.getElementById('bairro').value=ui.item.data.bairro;
            document.getElementById('cidade').value=ui.item.data.cidade;
            document.getElementById('estado').value=ui.item.data.estado;
            localStorage.setItem('endereco',true);
        },
        response: function(event, ui){
            if(ui.content.length == 0){
                //alert('Nada encontrado na sua busca');
                //$('#nomeouemail').addClass('has-danger');
            }else{
               //console.log('response',ui.content);
            }
        },
        search: function() {
            var termo = extractLast( this.value );
            if( termo.length < 1 ){
                return false;
            }
        }
    });

    $('.date').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: "pt-BR",
        keepOpen:false,
    });

    $('.dateend').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: "pt-BR",
        keepOpen:false,
        // minDate: new Date($('.date').val().replace(/-/g, "/")),
        // minDate: new Date($('.date').val()),
    });

});

var continua=true;
function obrigatorio(elem){
    if(elem.value == ""){
        elem.classList.remove('inputjaubra');
        elem.classList.add('inputjaubra_erro');
        continua=false;
    }else{
        elem.classList.remove('inputjaubra_erro');
        elem.classList.add('inputjaubra');
        continua=true;
    }
}

function validateEmail(email){
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}


function valida_cpf(cpf){
      var numeros, digitos, soma, i, resultado, digitos_iguais;
      digitos_iguais = 1;
      if (cpf.length < 11)
            return false;
      for (i = 0; i < cpf.length - 1; i++)
            if (cpf.charAt(i) != cpf.charAt(i + 1))
                  {
                  digitos_iguais = 0;
                  break;
                  }
      if (!digitos_iguais)
            {
            numeros = cpf.substring(0,9);
            digitos = cpf.substring(9);
            soma = 0;
            for (i = 10; i > 1; i--)
                  soma += numeros.charAt(10 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                  return false;
            numeros = cpf.substring(0,10);
            soma = 0;
            for (i = 11; i > 1; i--)
                  soma += numeros.charAt(11 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                  return false;
            return true;
            }
      else
            return false;
}

function validateDate(elem) {
    var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])      [\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;


    if( !((elem.value.match(RegExPattern)) && (elem.value!='')) ) {
        return false;
    }else{
        return true;
    }
}

function salvar(){

    //var camposobrig = [];
    var camposobrig = ['nome','email', 'cpf','senha', 'confirmasenha'];

    var inpts = document.getElementsByTagName('input');
    var in_ids = [];
    var in_values = [];

    for(var i=0; i<inpts.length; i++){
        var id = inpts[i].id;
        if(id != ""){
            if(id != "imagemsel"){
                for(var o in camposobrig){
                    if(inpts[i].id == camposobrig[o]){
                        obrigatorio(inpts[i]);
                    }
                }
            }
        }
    }

    if(document.getElementById('senha').value != document.getElementById('confirmasenha').value){
        alert('Confirme a senha correta');
        return false;
    }

    var cpf = document.getElementById('cpf').value;
    cpf = cpf.replace('.','');
    cpf = cpf.replace('.','');
    cpf = cpf.replace('-','');

    if( !valida_cpf(cpf) ){
        alert('Informe CPF válido');
        return false;
    }
    
    var data = document.getElementById('nascimento');

    if( !validateDate(document.getElementById('nascimento')) ){
        alert('Data de nascimento inválida');
        return false;
    }

    if(contemail){

        for(var i=0; i<inpts.length; i++){
            var id = inpts[i].id;
            if(id != ""){
                if(id != 'confirmasenha'){
                    if(id == "imagemsel"){
                        in_ids.push(id);
                        in_values.push({id:id, valor: inpts[i].value});
                    }else{
                        in_ids.push(id);
                        in_values.push(inpts[i].value);
                    }
                }
            }
        }

        var ds=[];
        for(var d in in_ids){
            ds[in_ids[d]] = in_values[d];
        }

        if(continua){
            document.getElementById("formcadastro").submit();
        }else{
            alert('Os campos em vermelho são obrigatórios');
        }
    }else{

        if( document.getElementById('email').value != document.getElementById('email_atual').value){

            if( validateEmail(document.getElementById('email').value) ){
                $.ajax({
                    type: "GET",
                    url: API_URL+'/web/emailexiste/'+document.getElementById('email').value,
                    success: function(ret){
                        if(ret.length == 1){
                            alert('Email já cadastrado');
                        }else{
                            for(var i=0; i<inpts.length; i++){
                                var id = inpts[i].id;
                                if(id != ""){
                                    if(id != 'confirmasenha'){
                                        if(id == "imagemsel"){
                                            in_ids.push(id);
                                            in_values.push({id:id, valor: inpts[i].value});
                                        }else{
                                            in_ids.push(id);
                                            in_values.push(inpts[i].value);
                                        }
                                    }
                                }
                            }

                            var ds=[];
                            for(var d in in_ids){
                                ds[in_ids[d]] = in_values[d];
                            }

                            if(continua){
                                document.getElementById("formcadastro").submit();
                            }else{
                                alert('Os campos em vermelho são obrigatórios');
                            }

                        }
                    },
                    error: function(e){

                    }
                });
            }else{
                alert('Digite email válido');
            }

        }else{
            for(var i=0; i<inpts.length; i++){
                var id = inpts[i].id;
                if(id != ""){
                    if(id != 'confirmasenha'){
                        if(id == "imagemsel"){
                            in_ids.push(id);
                            in_values.push({id:id, valor: inpts[i].value});
                        }else{
                            in_ids.push(id);
                            in_values.push(inpts[i].value);
                        }
                    }
                }
            }

            var ds=[];
            for(var d in in_ids){
                ds[in_ids[d]] = in_values[d];
            }

            if(continua){
                document.getElementById("formcadastro").submit();
            }else{
                alert('Os campos em vermelho são obrigatórios');
            }
        }
        
    }

}

</script>

</body>
</html>