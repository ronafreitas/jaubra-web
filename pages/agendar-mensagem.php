<?php
session_start();
ini_set('session.gc_maxlifetime', 80*80);
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}

?>
<!DOCTYPE html>
<html>

<head>
	<?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <!--<link href="<?=CSS;?>bootstrap-datepicker.min.css" rel="stylesheet">-->
    <link href="<?=CSS;?>bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style type="text/css">
        .sweet-alert.show-input input {
            display: block;
            font-weight: bold;
            color: #8e8c8c;
        }
    </style>
</head>

<body class="theme-teal">

    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    ENVIO DE MENSAGENS
                    <small>Utilize a interface abaixo para gerenciar o envio de mensagens aos clientes e motoristas</small>
                </h2>
            </div>

            <div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                              AGENDAR ENVIO DE MENSAGENS
                            </h2>
                        </div>

                        <form action="salvar-mensagem" method="post">
                        <div style="padding: 20px;">
                            <div class="form-group">
                                Selecione o perfil &nbsp;&nbsp;&nbsp; 
                                <input type="radio" name="perfil" value="motorista" checked id="motorista" class="with-gap">
                                <label for="motorista">Motorista</label>
                                <input type="radio" name="perfil" value="passageiro" id="passageiro" class="with-gap">
                                <label for="passageiro" class="m-l-20">Passageiro</label>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <div style="width: 50%">
                                    <label for="sexo">Sexo</label>
                                    <select class="form-control" id="sexo">
                                        <option value="masculino">Masculino</option>
                                        <option value="feminino">Feminino</option>
                                    </select>
                                </div> 
                            </div>
                            
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                <label>Idade</label><br/>
                                <div style="width: 40%; float: left;">
                                    <div class="input-group">
                                        <input maxlength="2" id="idadeinit" type="text" class="form-control idade" style="padding-left: 5px;border: 1px solid #cccccc;">
                                    </div>
                                </div>
                                <span style="float: left; padding: 5px">à</span>
                                 <div style="width: 40%; float: left;">
                                    <div class="input-group">
                                        <input maxlength="2" id="idadefim" type="text" class="form-control idade" style="padding-left: 5px;border: 1px solid #cccccc;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                        <div style="padding: 20px;">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <div style="width: 50%">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status">
                                        <option value="ativo">Ativo</option>
                                        <option value="inativo">Inativo</option>
                                    </select>
                                </div> 
                            </div>
                            
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                <label>Data de cadastro</label><br/>
                                
                                <div style="width: 40%; float: left;">
                                <div class="input-group date">
                                    <input id="datacadastroinit" type="text" class="form-control datamask" style="padding-left: 5px;border-left: 1px solid #cccccc; border-top:  1px solid #cccccc; border-bottom:  1px solid #cccccc;">
                                    <span class="input-group-addon" style="border-right: 1px solid #cccccc; border-top:  1px solid #cccccc; border-bottom:  1px solid #cccccc;">
                                        <i class="glyphicon glyphicon-th"></i>
                                    </span>
                                </div>
                                </div>
                                <span style="float: left; padding: 5px">à</span>
                                <div style="width: 40%; float: left; ">
                                <div class="input-group dateend">
                                    <input id="datacadastrofim" type="text" class="form-control datamask" style="padding-left: 5px;border-left: 1px solid #cccccc; border-top:  1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                                    <span class="input-group-addon" style="border-right: 1px solid #cccccc; border-top:  1px solid #cccccc; border-bottom:  1px solid #cccccc;">
                                        <i class="glyphicon glyphicon-th"></i>
                                    </span>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div style="padding: 20px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" margin-bottom: 10px;">
                                <div>
                                    <input type="radio" checked="" name="pesquisa" id="nome" class="with-gap tipopesquisa">
                                    <label for="nome">Nome</label>
                                    <input type="radio" name="pesquisa" id="email" class="with-gap tipopesquisa">
                                    <label for="email" class="m-l-20">Email</label>
                                    <input type="text" class="form-control" id="nomeouemail" style="padding-left: 5px;border-left: 1px solid #cccccc; border-top:  1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                                    <input type="hidden" id="nome_email_pesquisa">
                                </div> 
                            </div>
                        </div>

                        <div style="padding: 20px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" margin-bottom: 10px;">
                                <div>
                                    <label for="sel1">Título</label>
                                    <input autocomplete="off" id="titulo" type="text" class="form-control" style="padding-left: 5px;border-left: 1px solid #cccccc; border-top:  1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                                </div> 
                            </div>
                        </div>
                        <div style="padding: 20px;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" margin-bottom: 10px;">
                                <div>
                                    <label for="sel1">Mensagem</label>
                                    <textarea id="ckeditor">

                                    </textarea>
                                </div> 
                            </div>
                        </div>
                        </form>
                        <div class="body" style=" margin-top: 20px;">
                            <span class="btn btn-success btn-lg js-sweetalert waves-effect" data-type="prompt"><b>AGENDAR</b></span>
                            <span class="btn btn-danger btn-lg"><b>VOLTAR</b></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('../includes/footer.php'); ?>
<script src="<?=PLUGINS;?>sweetalert/sweetalert-dev.js"></script>
<script src="<?=PLUGINS;?>ckeditor/ckeditor.js"></script>
<script src="<?=PLUGINS;?>momentjs/moment.js"></script>
<script src="<?=JS;?>pt-br.js"></script>
<script src="<?=JS;?>bootstrap-datetimepicker.min.js"></script>
<script src="<?=JS;?>CONFIG.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=JS;?>jquery.mask.min.js"></script>

<script type="text/javascript">

var ckdados;

$(function(){

    $('.idade').mask('00');

    $(".datamask").on('input', function() {
        $(this).val('');
    });

    function split( val ) {
        return val.split( /,\s*/ );
    }

    function extractLast( term ) {
        return split( term ).pop();
    }

    $("input[type='radio']").on('change', function(){
        $('#nomeouemail').val('').focus();
    });

    $("#nomeouemail").autocomplete({
        source: function(request, response){
            var tp = $(".tipopesquisa:checked").attr('id');
            if(tp == 'nome'){
                $.getJSON(API_URL+"/getnomeouemail/"+request.term+"/"+tp, function (data) {
                    response($.map(data, function (value, key){
                        return {
                            label: value.nome,
                            value: value.nome,
                            id:value.id
                        };
                    }));
                });
            }else{
                $.getJSON(API_URL+"/getnomeouemail/"+request.term+"/"+tp, function (data) {
                    response($.map(data, function (value, key){
                        return {
                            label: value.email,
                            value: value.email,
                            id:value.id
                        };
                    }));
                });
            }
        },
        select: function(event, ui) {
            //console.log('select',ui.item);
            $('#nome_email_pesquisa').val(ui.item.id);
        },
        response: function(event, ui){
            if(ui.content.length == 0){
                //alert('Nada encontrado na sua busca');
                //$('#nomeouemail').addClass('has-danger');
            }else{
               //console.log('response',ui.content);
            }
        },
        search: function() {
            var termo = extractLast( this.value );
            if( termo.length < 1 ){
                return false;
            }
        }
    });

    var toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        '/',
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        
    ];

    var editor = CKEDITOR.replace( 'ckeditor',{
        toolbar : 'Basic',
        height:300,
        toolbarGroups,
    });
    
    editor.on( 'change', function( evt ) {
        ckdados = evt.editor.getData();
        //console.log( evt.editor.getData() );
    });

    $('.date').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: "pt-BR",
        keepOpen:false,
    });

    $('.dateend').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: "pt-BR",
        keepOpen:false,
        //minDate: new Date($('.date').val().replace(/-/g, "/")),
       // minDate: new Date($('.date').val()),
    });

    $('.js-sweetalert').on('click', function () {
        showPromptMessage();
    });
});

function showPromptMessage(){

    if(document.getElementById('titulo').value == ""){
        alert('Necessário escrever um título');
        return false;
    }

    if(document.getElementById('idadeinit').value == "" || document.getElementById('idadefim').value == ""){
        alert('Necessário digitar Idade');
        return false;
    }

    if(document.getElementById('datacadastroinit').value == "" || document.getElementById('datacadastrofim').value == ""){
        alert('Necessário selecionar Data de cadastro');
        return false;
    }

    var idadeinit = document.getElementById('idadeinit').value;
    var idadefim  = document.getElementById('idadefim').value;
    if( parseInt(idadeinit) == 0 || 
        parseInt(idadeinit) == 00 || 
        parseInt(idadeinit) == '0' || 
        parseInt(idadeinit) == '00' || 
        parseInt(idadefim) == 0 || 
        parseInt(idadefim) == 00 || 
        parseInt(idadefim) == '0' || 
        parseInt(idadefim) == '00'
    ){
        alert('Selecione a idade corretamente');
        return false;
    }

    var strData = document.getElementById('datacadastroinit').value;
    var endData = document.getElementById('datacadastrofim').value;
    var partesData1 = strData.split("/");
    var partesData2 = endData.split("/");
    var data1 = new Date(partesData1[2], partesData1[1] - 1, partesData1[0]);
    var data2 = new Date(partesData2[2], partesData2[1] - 1, partesData2[0]);
    if(data1 > data2){
       alert("A data inicial de cadastro deve ser menor ou igual que a data final");
       //document.getElementById('datacadastrofim').focus();
       return false;
    }

    var d = new Date();
    
    var hora = d.getHours();
    var minuto = d.getMinutes();
    
    if(hora < 10){
        hora = '0'+hora;
    }
    
    if(minuto < 10){
        minuto = '0'+minuto;
    }
    
    var horacompleta = hora+':'+minuto;

    var dia = d.getDate();
    var mes = d.getMonth();
    mes = mes+1;
    if(mes < 9){
        mes ='0'+mes;
    }
    var ano = d.getFullYear();
    var datacompleta = dia+'/'+mes+'/'+ano;
    
    setTimeout(function(){
        $('#dataenvio').val(datacompleta); 
        $('.inputcustom').val(horacompleta); 
    }, 300);
    
    swal({
        title: "Agendar envio",
        html: true,
        text: "Selecione a data de envio:"+
        "<input style='font-weight: bold;' id='dataenvio' type='text' class='dateenv'><br>Hora do envio: ",                
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: false,
        animation: "slide-from-top",
    }, function (inputValue){

        if(!inputValue){
            return false;
        }

        var data1 = inputValue;
        var data2 = new Date();
        var mes = (data2.getMonth()+1);
        var horaat   = data2.getHours();
        var minutoat = data2.getMinutes();
        if(mes < 10){
            mes = '0'+mes;
        }
        var dia = data2.getDate();
        if(dia < 10){
            dia = '0'+dia;
        }
        var dataatual  = parseInt(data2.getFullYear()+''+mes+''+dia);
        var dataselect = parseInt(data1.split("/")[2].toString() + data1.split("/")[1].toString() + data1.split("/")[0].toString());

        var data = $('#dataenvio').val();
        var hora = $('.inputcustom').val();

        if(data == ""){
            $('#dataenvio').click();
            return false;
        }

        if(hora == ""){
            $('.inputcustom').focus();
            return false;
        }

        var formhra = horaat+':'+minutoat;
        var hora1 = hora.split(":");
        var hora2 = formhra.split(":");

        var d = new Date();
        var hrfodata1 = new Date(d.getFullYear(), d.getMonth(), d.getDate(), hora1[0], hora1[1]);
        var hrfodata2 = new Date(d.getFullYear(), d.getMonth(), d.getDate(), hora2[0], hora2[1]);

        if(hrfodata1 <= hrfodata2){
            alert('A hora do agendamento deve ser maior que a hora atual');
            return false;
        }

        if(dataatual > dataselect){
            alert('A data do agendamento deve ser igual ou maior que a data atual');
            return false;
        }

        var perfil = document.querySelector('input[name="perfil"]:checked').value;
        var sexo   = document.getElementById('sexo').value;
        var titulo = document.getElementById('titulo').value;
        var texto  = ckdados;
        var status = document.getElementById('status').value;
        var datacadastroinit = document.getElementById('datacadastroinit').value;
        var datacadastrofim  = document.getElementById('datacadastrofim').value;
        var idadeinit = document.getElementById('idadeinit').value;
        var idadefim  = document.getElementById('idadefim').value;
        var id_pesquisa = document.getElementById('nome_email_pesquisa').value;
        
        if( parseInt(idadeinit) >  parseInt(idadefim)){
            alert('A idade inicial deve ser menor que a idade final');
            return false;
        }
        if(!id_pesquisa){
            id_pesquisa = "";
        }
        var dados = {
            titulo:titulo,
            texto:texto,
            perfil:perfil,
            sexo:sexo,
            idadeinit:idadeinit,
            idadefim:idadefim,
            status:status,
            id_pesquisa:id_pesquisa,
            datacadastroinit:datacadastroinit,
            datacadastrofim:datacadastrofim,
            data_agenda:data,
            hora_agenda:hora
        };

        //console.log(dados);

        $.ajax({
          type: "POST",
          //dataType: 'json',
          url: API_URL+'/web/gravarmensagem',
          data: {dados:dados},
          success: function(ret){
            console.log('d',ret);

            if(ret.success){
                swal({
                    title: "Gravado com sucesso",
                    text: "Sua mensagem foi agendada corretamente",
                    timer: 4000,
                    showConfirmButton: false
                }, function () {
                   location.href = './mensagens';
                });
            }else{
                swal({
                    title: "Ops!",
                    text: "Não foi possível gravar, tente novamente",
                    timer: 4000,
                    showConfirmButton: true
                }, function () {
                    //swal.close();
                   // alert('oookkk');
                });
            }
          },
          error: function(e){
            
            swal({
                title: "Ops!",
                text: "Não foi possível gravar, tente novamente",
                timer: 4000,
                showConfirmButton: false
            }, function () {
                console.log('e',e);
               // alert('oookkk');
            });
          }

        });

    });

    $('#dataenvio').on('input', function(){
        $('.dateenv').datetimepicker('show');
        $(this).val('');
    });
   
    $('#dataenvio').on('click', function(){
        $('.dateenv').datetimepicker('show');
        $('.sweet-alert').css({'height':'350px'});
    });

    setTimeout(function(){

        $('.dateenv').datetimepicker({
            format: 'DD/MM/YYYY',
            locale: "pt-BR",
            keepOpen:false,
        });

        $('.inputcustom').focusin(
          function(){  
            $('.sweet-alert').css({'height':'500px'}); 
          }).focusout(  
          function(){  
            $('.sweet-alert').css({'height':'350px'}); 
        });

        $('.inputcustom').datetimepicker({
          format: "HH:mm",
          showClose:true,
          keepOpen:false,
        });

    }, 400);
}
</script>

</body>
</html>