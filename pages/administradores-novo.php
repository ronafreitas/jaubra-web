<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?=CSS;?>bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?=CSS;?>bootstrap-datetimepicker.min.css" rel="stylesheet">
</head>

<body class="theme-teal">
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2> CADASTRAR ADMINISTRADOR </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                    <div style="width:50%">
                    <form method="post" action="administradores-salvar">
	                    <div class="body">
                        
							<div class="form-group">
								<label for="assunto">
									Nome
								</label>
								<input autocomplete="off" class="form-control inputjaubra" required name="nome" type="text" />
							</div>
							<div class="form-group">
								<label for="assunto">
									Email
								</label>
								<input autocomplete="off" class="form-control inputjaubra" required name="email" type="email" />
							</div>
                            <div class="form-group">
								<label for="assunto">
									Senha
								</label>
								<input autocomplete="off" class="form-control inputjaubra" required name="senha" type="text" />
							</div>
                            <button class="btn btn-primary">Salvar</button>

                            <div id="load"></div>
	                    </div>
                        </form>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

<?php include('../includes/footer.php'); ?>


</body>
</html>