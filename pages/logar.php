<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if( isset($_POST['email_login']) and isset($_POST['senha_login']) ){

		if (!filter_var($_POST["email_login"], FILTER_VALIDATE_EMAIL)) {
		  header('Location: '.HOST.'login/emailerror');
		}

		session_start();
		require_once('../includes/defines.php');

		$url = API_URL_WEB.'login_web';
		$fields = array(
			'email_login' => urlencode($_POST['email_login']),
			'senha_login' => urlencode($_POST['senha_login'])
		);
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		$res = json_decode($result,true);

		if( isset($res['error']) ){
			header('Location: '.HOST.'login/errorlogin');
		}else{
			$_SESSION['logado'] = true;
			$_SESSION['jadm'] = $res['jadm'];
			$_SESSION['per']  = $res['per'];
			header('Location: '.HOST.'home');
		}
	}else{
		header('Location: '.HOST.'login/');
	}
}else{
	header('Location: '.HOST);
}
