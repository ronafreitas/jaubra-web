<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}

$url = API_URL_WEB.'administradores';
$json = file_get_contents($url);
$admins = json_decode($json);
?>
<!DOCTYPE html>
<html>

<head>
    <?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?=PLUGINS;?>jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?=CSS;?>bootstrap-datepicker.min.css" rel="stylesheet">
    <style type="text/css">
        .well{
            box-shadow: 3px 2px 0px 1px #c3c7dc;
            background-color: #d7d9e3;
        }
        .titunmemp{
            font-size: 20px;
            border-radius: 5px;
            color: #fff;
            padding: 6px;
            background-color: #a1a7c7;
        }
    </style>
</head>

<body class="theme-teal">

    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                  Administradores
                    <br/>
                    <br/>
                </h2>
            </div>
            
           
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                              LISTA DE ADMINISTRADORES
                            </h2>
                        </div>
                        <div class="body">
                            <div id="cadasuce" style="display: none" class="alert alert-success">
                                Ação realizada com sucesso.
                            </div>
                            <div id="cadasterro" style="display: none" class="alert alert-danger">
                                Não foi possível realizar essa ação.
                            </div>
                            <div>
                            <!--<div class="table-responsive">-->
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Email</th>
                                            <th>Perfil</th>
                                            <th>Operação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($admins as $adm):?>
                                            <tr>
                                                <td><?=$adm->nome;?></td>
                                                <td><?=$adm->email;?></td>
                                                <td>
                                                <?php if($adm->tipo != 0){?>
                                                    Admin secundário
                                                <?php }else{;?>
                                                   <b> Admin principal</b>
                                                <?php };?>
                                                </td>
                                                <td>
                                                    <a href="administradores-editar/<?=$adm->id;?>" class="btn btn-success btn-xs">Editar</a>
                                                    <?php if($adm->tipo != 0):?>
                                                        <a onclick="return apagar(<?php echo "'".$adm->nome."'";?>,<?=$adm->id;?>);" href="javascript:void(0)" class="btn btn-danger btn-xs">Apagar</a>
                                                    <?php endif;?>
                                                </td>
                                            </tr>    
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <br/>
                            <a href="administradores-novo" class="btn btn-success btn-lg"><b>NOVO ADMINISTRADOR</b></a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

<?php include('../includes/footer.php'); ?>

<script src="<?=PLUGINS;?>sweetalert/sweetalert.min.js"></script>
<script src="<?=PLUGINS;?>jquery-datatable/jquery.dataTables.js"></script>
<script src="<?=PLUGINS;?>jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?=JS;?>bootstrap-datepicker.min.js"></script>
<script src="locales/bootstrap-datepicker.pt-BR.min.js"></script>

<script type="text/javascript">

    $(function(){

        $.ajax({
            type: "GET",
            url: 'async/checkSession?s=confirma_cadastro',
            success: function(ret){
                if(ret != 'none'){
                    if(ret == 'true'){
                        $('#cadasuce').show();
                        setTimeout(function(){
                            $('#cadasuce').hide();
                        }, 3000);
                    }else{
                        $('#cadasterro').show();
                        setTimeout(function(){
                            $('#cadasterro').hide();
                        }, 3000);
                    }
                }
            },
            error: function(e){

            }
        });

        $('.input-group.date').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true,
            orientation: "bottom right"
        });

        $('.js-basic-example').DataTable({
            responsive: true,
            "lengthChange": false,
            "searching": true,
            language: {
                processing:     "Aguarde...",
                search:         "Buscar:",
                info:           "Exibindo _START_ de _END_",
                zeroRecords:    "Nenhum resultado encontrado",
                emptyTable:     "Não existem dados para serem exibidos",
                paginate: {
                    first:      "Início",
                    previous:   "Anterior",
                    next:       "Próximo",
                    last:       "Fim"
                }
            }
        });
    });
    
    function apagar(nome, id){
        var del = confirm("Deseja mesmo apagar '"+nome+"'?");
        if(del == true){
            location.href='administradores-apagar/'+id;
        }
    }

</script>
</body>
</html>