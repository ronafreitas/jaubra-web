<?php
//echo "<pre>"; print_r($_POST); exit;

if( isset($_POST) ){
	
	require_once('../includes/defines.php');
	require_once('../classes/RequestApi.class.php');
	
	if( isset($_POST['editar']) ){
		$_POST['cadastro']['foto'] = $_POST['cadastro']['foto_atual'];
		if( isset($_FILES) ){
			$uploaddir  = PROFILE_UPLOAD;

			$name = $_FILES["imagem"]["name"];
			$ext = end((explode(".", $name)));
			$nomearquivo = time().'.'.$ext;
			$uploadfile = $uploaddir.$nomearquivo;

			if(move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)){
			   $_POST['cadastro']['foto'] = $nomearquivo;
			}
		}

		$ret = RequestApi::POST(API_URL_WEB.'editacadastro', $_POST['cadastro']);

		if($ret[0]['success']){
			if( isset($_POST['endereco']['cep']) ){
				
				$del_end = RequestApi::DELETE(API_URL_WEB.'deleletarenderecobyidusuario/'.$_POST['cadastro']['id']);
				//RequestApi::DELETE

				//echo "<pre>";print_r($del_end);	exit;

				$_POST['endereco']['id'] = $_POST['cadastro']['id'];
				$ret_end = RequestApi::POST(API_URL_WEB.'gravaenderecos', $_POST['endereco']);
				if($ret_end[0]['success']){
					session_start();
					$_SESSION['confirma_cadastro']=true;
					header('Location: dadoscadastrais');
				}else{
					session_start();
					$_SESSION['confirma_cadastro']=false;
					header('Location: dadoscadastrais');
				}
			}else{
				session_start();
				$_SESSION['confirma_cadastro']=true;
				header('Location: dadoscadastrais');
			}
		}else{
			session_start();
			$_SESSION['confirma_cadastro']=false;
			header('Location: dadoscadastrais');
		}


	}else{

		$_POST['cadastro']['foto'] = '';
		if( isset($_FILES) ){
			$uploaddir  = PROFILE_UPLOAD;

			$name = $_FILES["imagem"]["name"];
			$ext = end((explode(".", $name)));
			$nomearquivo = time().'.'.$ext;
			$uploadfile = $uploaddir.$nomearquivo;

			if(move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)){
			   $_POST['cadastro']['foto'] = $nomearquivo;
			}
		}

		$ret = RequestApi::POST(API_URL_WEB.'gravacadastro', $_POST['cadastro']);

		if($ret[0]['success']){
			if( isset($_POST['endereco']) ){
				$_POST['endereco']['id'] = $ret[0]['li'];
				$ret_end = RequestApi::POST(API_URL_WEB.'gravaenderecos', $_POST['endereco']);
				if($ret_end[0]['success']){
					session_start();
					$_SESSION['confirma_cadastro']=true;
					header('Location: dadoscadastrais');
				}else{
					session_start();
					$_SESSION['confirma_cadastro']=false;
					header('Location: dadoscadastrais');
				}
			}else{
				session_start();
				$_SESSION['confirma_cadastro']=true;
				header('Location: dadoscadastrais');
			}
		}else{
			session_start();
			$_SESSION['confirma_cadastro']=false;
			header('Location: dadoscadastrais');
			/*
			(
			    [0] => Array
			        (
			            [error] => 3
			            [exception] => Array
			                (
			                    [errorInfo] => Array
			                        (
			                            [0] => 22001
			                            [1] => 1406
			                            [2] => Data too long for column 'cpf' at row 1
			                        )

			                )
			            [query] => INSERT INTO `usuarios`....
			            [message] => Erro inesperado, tente novamente.
			        )
			*/
		}
	}

}else{
	header('Location: dadoscadastrais-novo');
}