<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
if( isset($_GET['id']) ){
	require_once('../includes/defines.php');
	require_once('../classes/RequestApi.class.php');
    $dele = RequestApi::DELETE(API_URL_WEB.'deletaadmin/'.$_GET['id']);
    $d = json_decode($dele[0]);
    if($d->success){
        $_SESSION['confirma_cadastro']=true;
        header('Location: ../administradores');
    }else{
        $_SESSION['confirma_cadastro']=false;
        header('Location: ../administradores');
    }
}else{
	header('Location: ../administradores');
}