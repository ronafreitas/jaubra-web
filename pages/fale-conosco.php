<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php 
    include('../includes/header.php'); 
    require_once('../classes/Base.class.php');
    ?>
    <link href="<?=PLUGINS;?>sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?=CSS;?>bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?=CSS;?>bootstrap-datetimepicker.min.css" rel="stylesheet">
</head>

<body class="theme-teal">
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <?php include('../includes/navbar.php'); ?>
    <section>
        <aside id="leftsidebar" class="sidebar">
            <?php include('../includes/menu.php'); ?>
        </aside>
        <aside id="rightsidebar" class="right-sidebar">
            <?php include('../includes/sidebar-right.php'); ?>
        </aside>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                   CONTATO
                    <small>Olá, para entrar em contato com a Juabra, preencha o formulário abaixo ou entre em contato por telefone.</small>
                </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
	                    <div class="body">
	                       <!--projetos@faromidia.com.br-->
		
							<div class="form-group">
								<label for="assunto">
									Assunto
								</label>
								<input class="form-control inputjaubra" id="assunto" type="email" />
							</div>
							<div class="form-group">
								<label for="descricao">
									Descrição
								</label>
								<textarea id="descricao" class="form-control inputjaubra"  ></textarea>
							</div>
						 
							<span onclick="return enviar()" class="btn btn-primary">
								Enviar
							</span>
                            <div id="load"></div>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    
</section>

<?php include('../includes/footer.php'); ?>
<script src="<?=PLUGINS;?>sweetalert/sweetalert-dev.js"></script>

<script src="<?=PLUGINS;?>momentjs/moment.js"></script>
<script src="<?=JS;?>pt-br.js"></script>
<script src="<?=JS;?>bootstrap-datetimepicker.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=JS;?>jquery.mask.min.js"></script>
<script src="<?=JS;?>CONFIG.js"></script>

<script type="text/javascript">

function obrigatorio(elem){
    if(elem.value == ""){
        elem.classList.remove('inputjaubra');
        elem.classList.add('inputjaubra_erro');
        continua=false;
    }else{
        elem.classList.remove('inputjaubra_erro');
        elem.classList.add('inputjaubra');
        continua=true;
    }
}


function enviar(){
	var assunto = document.getElementById('assunto').value;
	var descricao = document.getElementById('descricao').value;
    if(assunto == ""){
        obrigatorio(document.getElementById('assunto'));
        alert('Os dois campos são obrigatórios');
        return false;
    }
    if(descricao == ""){
        obrigatorio(document.getElementById('descricao'));
        alert('Os dois campos são obrigatórios');
        return false;
    }

    $.ajax({
        type: "POST",
        url: 'enviar-contato',
        data: 'a='+assunto+'&d='+descricao,
        beforeSend:function(d){
        	$('#load').html('<span class="alert alert-warning">Aguarde...</span>');
        },
        success: function(ret){
            if(ret == '1'){
                $('#load').html('<span class="alert alert-success">Mensagem enviada.</span>');
                setTimeout(function(){
                    $('#load').html('');
                }, 3000);
            }else{
                $('#load').html('<span class="alert alert-danger">Mensagem não enviada, tente novamente.</span>');
                setTimeout(function(){
                    $('#load').html('');
                }, 3000);
            }
        	document.getElementById('assunto').value='';
        	document.getElementById('descricao').value='';
        },
        error: function(e){

        }
    });
}
</script>

</body>
</html>