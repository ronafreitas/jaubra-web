<?php
ini_set('session.gc_maxlifetime', 80*80);
session_start();
require_once('../includes/defines.php');
if(!$_SESSION){
  header('Location: '.HOST);
}
if( isset($_POST['email']) AND isset($_POST['nome']) AND isset($_POST['senha']) ){
	
	require_once('../includes/defines.php');
	require_once('../classes/RequestApi.class.php');
	
	if( isset($_POST['editar']) ){
        $ret = RequestApi::POST(API_URL_WEB.'editaadministradores', $_POST);
		if($ret[0]['success']){
            $_SESSION['confirma_cadastro']=true;
            header('Location: administradores');
		}else{
			$_SESSION['confirma_cadastro']=false;
			header('Location: administradores');
        }
	}else{
		$ret = RequestApi::POST(API_URL_WEB.'gravaadministradores', $_POST);
		if($ret[0]['success']){
            $_SESSION['confirma_cadastro']=true;
            header('Location: administradores');
		}else{
			$_SESSION['confirma_cadastro']=false;
			header('Location: administradores');
        }
	}
}else{
	header('Location: administradores-novo');
}